package it.uniroma1.metodologie2018.javabomber.world;

import java.util.Comparator;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Config;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.GraphicEntity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldRenderer;

public class JavaBomberWorldRenderer implements WorldRenderer{

	private WorldController controller;
	private Batch batch;
	private JavaBomber game; //AGGIUNTO
	private OrthographicCamera gamecam; //AGGIUNTO
	private Viewport gamePort; //AGGIUNTO
	private Box2DDebugRenderer b2dr; //AGGIUNTO
	

	public JavaBomberWorldRenderer(JavaBomber game, WorldController worldController) {
		Config.setWorldRenderer(this);
		b2dr = new Box2DDebugRenderer(); //AGGIUNTO
		gamecam = new OrthographicCamera(); //AGGIUNTO
		gamePort = new FitViewport(480,416,gamecam); //AGGIUNTO
		gamecam.position.set(gamePort.getWorldWidth() / 2,gamePort.getWorldHeight() / 2, 0);  //AGGIUNTO
		controller = worldController;
		batch = game.getBatch();
		this.game = game; //AGGIUNTO
	}

	@Override
	public void dispose() {
		batch.dispose();
	}

	@Override
	public void render() {
		batch.begin();
		controller.getEntities().sort(Comparator.comparing(Entity::getZindex));
		for(Entity e : controller.getEntities())
			if (e instanceof GraphicEntity)
				((GraphicEntity) e).draw(batch);
		batch.end();
		game.getBatch().setProjectionMatrix(gamecam.combined); //AGGIUNTO
		//b2dr.render(controller.getWorld(), gamecam.combined); //AGGIUNTO
	}

	@Override
	public void resize(int width, int height) {
		gamePort.update(width, height); //AGGIUNTO

	}
	
	//AGGIUNTO
	@Override
	public OrthographicCamera getCamera() {
		return gamecam;
	}
	

}
