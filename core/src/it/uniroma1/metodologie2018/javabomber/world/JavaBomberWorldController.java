package it.uniroma1.metodologie2018.javabomber.world;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.Level;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.screens.PlayScreen;

public class JavaBomberWorldController implements WorldController{

	private JavaBomber game;
	private List<Entity> entities;
	
	private World world; // AGGIUNTO
	private Level currentLevel; // AGGIUNTO

	public JavaBomberWorldController(JavaBomber game) {
		this.game = game;
		entities = new CopyOnWriteArrayList<>(); // Sostituito ArrayList con CopyOnWriteArrayList
		world = new World(new Vector2(0,0), true); //AGGIUNTO
	}

	@Override
	public synchronized void update(float deltaTime) {
		for(Entity e : entities)
			 e.update(deltaTime);
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public synchronized void addEntity(Entity e) {
		entities.add(e);
	}

	@Override
	public synchronized void removeEntity(Entity e) {
		entities.remove(e);
	}

	@Override
	public synchronized List<Entity> getEntities() {
		return entities;
	}

	@Override
	public Level getLevel() {
		return currentLevel;
	}
	
	@Override
	public void setLevel(Level currentLevel) {
		this.currentLevel = currentLevel;
	}

	//AGGIUNTO
	@Override
	public World getWorld() {
		return world;
	}
	
	//AGGIUNTO
	@Override
	public void playerDead(int lives) {
		this.game.setScreen(new PlayScreen(game, lives));
	}
}
