package it.uniroma1.metodologie2018.javabomber.maps;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.interfaces.Block;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.Map;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.powerup.PowerUpSpawner;

public class Mappa implements Map {
	private Body body;
	private WorldController worldController;
	private TmxMapLoader mapLoader;
	private TiledMap map;
	private OrthogonalTiledMapRenderer mapRenderer;
	private List<Block> blocks;
	
	public Mappa(WorldController worldController) {
		this.worldController = worldController;
		mapLoader = new TmxMapLoader();
		map = mapLoader.load("mappa.tmx");
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		
		blocks = new ArrayList<>();
		
		
		//muri indistruttibili
		for(MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();
			
			Block current = new HardBlock(worldController, new Vector2(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2), rect, map);
			blocks.add(current);
		}
		// Blocchi distruttibili
		for(MapObject object : map.getLayers().get(3).getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();
			
			Block current = new BrickWall(worldController, new Vector2(rect.getX() + rect.getWidth() / 2, rect.getY() + rect.getHeight() / 2), rect, map, this);
			blocks.add(current);
		}
	}
	
	public OrthogonalTiledMapRenderer getMapRenderer() {
		return mapRenderer;
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Vector2 getSize() {
		return new Vector2(15,13);
	}

	@Override
	public List<Block> getBlocks() {
		return blocks;
	}

	@Override
	public void putEntity(Entity e, Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Entity getBlock(Vector2 pos) {
		for(Entity e : blocks)
			if(toTilePos(e.getPosition()).equals(toTilePos(pos))) return e;
		return null;
	}
	
	private Vector2 toTilePos(Vector2 pos) {
		int tileX = (int)Math.ceil(pos.x / JavaBomber.TILE_SIZE);
		int tileY = (int)Math.ceil(pos.y / JavaBomber.TILE_SIZE);
		if(pos.x % JavaBomber.TILE_SIZE == 0) tileX++;
		if(pos.y % JavaBomber.TILE_SIZE == 0) tileY++;
		return new Vector2(tileX, tileY);
	}

	@Override
	public Entity getEntity(Vector2 pos) {
		// TODO Auto-generated method stub
		return null;
	}

}
