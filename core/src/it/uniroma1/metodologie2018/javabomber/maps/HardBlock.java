package it.uniroma1.metodologie2018.javabomber.maps;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Blocco;
import it.uniroma1.metodologie2018.javabomber.entities.Config;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

public class HardBlock extends Blocco {
	private Body body;
	private Fixture fixture;
	private TiledMap map;
	
	public HardBlock(WorldController controller, Vector2 pos, Rectangle rect, TiledMap map) {
		super(controller, pos);
		
		this.map = map;
		
		BodyDef bdef = new BodyDef();
		PolygonShape shape = new PolygonShape();
		FixtureDef fdef = new FixtureDef();
		
		
		bdef.type = BodyDef.BodyType.StaticBody;
		bdef.position.set(pos);
		
		body = controller.getWorld().createBody(bdef);
		
		shape.setAsBox(rect.getWidth() / 2, rect.getHeight() / 2);
		fdef.filter.categoryBits = JavaBomber.BLOCK_BIT;
		fdef.filter.maskBits = JavaBomber.BOMB_BIT | JavaBomber.PLAYER_BIT | JavaBomber.EXPLOSION_BIT | JavaBomber.ENEMY_BIT;
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	public void setCategoryFilter(short filterBit) {
		Filter filter = new Filter();
		filter.categoryBits = filterBit;
		fixture.setFilterData(filter);
	}
	
	@Override
	public void draw(Batch batch) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Body getBody() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void collide(Entity e) {
		
	}
	
	@Override
	public void destroy() {

	}
	
	private TiledMapTileLayer.Cell getCell() {
		TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(1);
		return layer.getCell((int)(body.getPosition().x / JavaBomber.TILE_SIZE), (int)(body.getPosition().y / JavaBomber.TILE_SIZE));
	}
	
	@Override
	public int getZindex() {
		return Config.BLOCK_Z_INDEX;
	}
}
