package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.GraphicEntity;

/**
 * Classe che definisce l'esplosione della bomba
 * @author Mauro
 *
 */
public class Explosion extends GameEntity implements GraphicEntity {
	
	/**
	 * enum che elenca le parti che compongono l'esplosione completa
	 * @author Mauro
	 *
	 */
	public enum ExplosionTile {
		CENTER, UP, DOWN, LEFT, RIGHT, EDGE;
	}
	
	/**
	 * Builder per la calsse Explosion
	 * @author Mauro
	 *
	 */
	public static class ExplosionBuilder extends GameEntityBuilder<ExplosionBuilder>{
		/**
		 * Stato iniziale dell'animazione
		 */
		private AnimationState initialState;
		/**
		 * quale parte dell'esplosione � rappresentata da questa entit�
		 */
		private ExplosionTile tile;
		/**
		 * il TextureAtlas da usare
		 */
		private TextureAtlas atlas;
		
		@Override
		protected ExplosionBuilder self() {
			return this;
		}
		
		/**
		 * Metodo set per il campo animationState
		 * @param initialState lo stato iniziale dell'animazione
		 * @return un oggetto ExplosionBuilder
		 */
		public ExplosionBuilder setAnimationState(AnimationState initialState) {
			this.initialState = initialState;
			return this;
		}
		
		/**
		 * Metodo set per il campo tile
		 * @param tile la parte dell'esplosione che si vuole definire con questa entit�
		 * @return un oggetto ExplosionBuilder
		 */
		public ExplosionBuilder setTile(ExplosionTile tile) {
			this.tile = tile;
			return this;
		}
		
		/**
		 * Metodo set per il campo atlas
		 * @param atlas il TextureAtlas da usare
		 */
		public ExplosionBuilder setTextureAtlas(TextureAtlas atlas) {
			this.atlas = atlas;
			return this;
		}

		/**
		 * Metodo build che restituisce l'oggetto Explosion con le caratteristiche selezionate
		 * @return oggetto Explosion
		 */
		public Explosion build() {
			Explosion instance = new Explosion();
			if (atlas == null) atlas = new TextureAtlas(Gdx.files.internal("javabomber_texture_pack.pack"));
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.tile = this.tile;
			instance.animation = new DecoratorAnimatedEntity(instance, initialState, atlas);
			instance.defineEntity();
			return instance;
		}

	}
	
	/**
	 * Decorato che descrive l'animazione di questa entit�
	 */
	private DecoratorAnimatedEntity animation;
	/**
	 * campo che indica la parte dell'esplosione che questa entit� rappresenta
	 */
	private ExplosionTile tile;
	
	
	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + animation.getSprite().getWidth() / 2, pos.y + animation.getSprite().getHeight() / 2);
		bdef.type = BodyDef.BodyType.DynamicBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		//shape.setAsBox(animation.getSprite().getWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getHeight() / 2 - JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.EXPLOSION_BIT;
		fdef.filter.maskBits = JavaBomber.BOMB_BIT | JavaBomber.BLOCK_BIT | JavaBomber.PLAYER_BIT | JavaBomber.POWER_UP_BIT | JavaBomber.ENEMY_BIT;
		fdef.shape = defineShape(shape);
		fdef.isSensor = true;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	/**
	 * Metodo ausiliario utilizzato per definire la forma della fixture
	 * @param shape la shape da modellare
	 * @return la shape modificata in relazione al campo tile
	 */
	private PolygonShape defineShape(PolygonShape shape) {
		switch(tile) {
		case CENTER:	shape.setAsBox(animation.getSprite().getWidth() / 2, animation.getSprite().getHeight() / 2, new Vector2(0,0), 45f);break;
		case UP:		shape.setAsBox(animation.getSprite().getWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getHeight() / 2);break;
		case DOWN: 		shape.setAsBox(animation.getSprite().getWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getHeight() / 2);break;
		case LEFT:		shape.setAsBox(animation.getSprite().getWidth() / 2, animation.getSprite().getHeight() / 2 - JavaBomber.TOLERANCE);break;
		case RIGHT:		shape.setAsBox(animation.getSprite().getWidth() / 2, animation.getSprite().getHeight() / 2 - JavaBomber.TOLERANCE);break;
		case EDGE:		shape.setAsBox(animation.getSprite().getWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getHeight() / 2 - JavaBomber.TOLERANCE);break;
		}
		return shape;
	}
	
	@Override
	public void update(float deltaTime) {
		animation.animate(deltaTime);
		if(animation.isCurrentAnimationEnded()) {
			controller.removeEntity(this);
			setCategoryFilter(JavaBomber.DESTROYED_BIT);
			//body.destroyFixture(fixture);
			controller.getWorld().destroyBody(body);
		}
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Body getBody() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void draw(Batch batch) {
		animation.getSprite().draw(batch);
	}

	@Override
	public void collide(Entity e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int getZindex() {
		return Config.EXPLOSION_Z_INDEX;
	}
	
	
}
