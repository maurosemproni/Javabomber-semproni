package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.interfaces.BombDropper;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * Classe che definisce l'oggetto bomba
 * @author Mauro
 *
 */
public class Bomb extends GameEntity {

	/**
	 * Classe builder per l'oggetto Bomb
	 * @author Mauro
	 *
	 */
	public static class BombBuilder extends GameEntityBuilder<BombBuilder>{
		
		/**
		 * Il personaggio che ha piazzato questa bomba
		 */
		private BombDropper owner;
		/**
		 * Spritesheet che si desidera usare per la bomba
		 */
		private TextureAtlas atlas;
		
		@Override
		protected BombBuilder self() {
			return this;
		}
		
		/**
		 * metodo set per il campo owner
		 * @param owner Il personaggio che ha piazzato questa bomba
		 * @return un oggetto di tipo BombBuilder
		 */
		public BombBuilder setOwner(BombDropper owner) {
			this.owner = owner;
			return this;
		}
		
		/**
		 * metodo set per il campo atlas
		 * @param atlas Spritesheet che si desidera usare per la bomba
		 * @return un oggetto di tipo BombBuilder
		 */
		public BombBuilder setTextureAtlas(TextureAtlas atlas) {
			this.atlas = atlas;
			return this;
		}
		
		/**
		 * Metodo che costruisce l'oggetto finale
		 * @return un oggetto di class Bomb
		 */
		public Bomb build() {
			Bomb instance = new Bomb();
			if (atlas == null) atlas = Config.BOMBERMAN_ATLAS;
			instance.timer = Bomb.TIMER_DURATION;
			instance.bTimerFreezed = false;
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.owner = this.owner;
			instance.animation = new DecoratorAnimatedEntity(instance, AnimationState.BOMB_PLACED, atlas);
			instance.defineEntity();
			return instance;
		}

	}
	
	/**
	 * Costante che definisce la durata del timer della bomba
	 */
	public static final float TIMER_DURATION = 3.0f;
	/**
	 * timer della bomba
	 */
	private float timer;
	/**
	 * Booleano che determina se il timer della bomba sia attivo o fermo 
	 */
	private boolean bTimerFreezed;
	
	/**
	 * Il decorator che definisce l'animazione della bomba
	 */
	private DecoratorAnimatedEntity animation;
	
	/**
	 * riferimento al personaggio che ha piazzato la boma
	 */
	private BombDropper owner;

	
	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + animation.getSprite().getWidth() / 2, pos.y + animation.getSprite().getHeight() / 2);
		bdef.type = BodyDef.BodyType.KinematicBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(animation.getSprite().getRegionWidth() / 2, animation.getSprite().getRegionHeight() / 2);
		fdef.filter.categoryBits = JavaBomber.BOMB_JUST_PLACED_BIT;
		fdef.filter.maskBits = JavaBomber.PLAYER_BIT | JavaBomber.BLOCK_BIT | JavaBomber.ENEMY_BIT | JavaBomber.EXPLOSION_BIT;
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	@Override
	public void update(float deltaTime) {
		animation.animate(deltaTime);
		animation.getSprite().setPosition(body.getPosition().x - animation.getSprite().getWidth() / 2, body.getPosition().y - animation.getSprite().getHeight() / 2);
		//pos = body.getPosition();
		
		if(timer <= 0.0f && !bTimerFreezed) explode();
		else timer -= deltaTime;
		
		GameEntity entity = (GameEntity) owner;
		
		if(fixture.getFilterData().categoryBits == JavaBomber.BOMB_JUST_PLACED_BIT)
			if(	entity.getPosition().x > pos.x + animation.getSprite().getWidth()  ||
				entity.getPosition().x < pos.x - animation.getSprite().getWidth()  ||
				entity.getPosition().y > pos.y + animation.getSprite().getHeight() ||
				entity.getPosition().y < pos.y - animation.getSprite().getHeight()  )
					setCategoryFilter(JavaBomber.BOMB_BIT);
	}
	
	/**
	 * Definisce il comportamento della bomba all'atto dell'esplosione
	 */
	private synchronized void explode() {
		setCategoryFilter(JavaBomber.DESTROYED_BIT);
		ExplosionsManager manager = null;
		if(owner instanceof Terminator) manager = new ExplosionsManager(controller, new TextureAtlas(Gdx.files.internal("boss.pack"))); 
		else manager = new ExplosionsManager(controller);
		manager.boom(controller, pos, owner.getExplosionExtent());
		owner.restoreBombCounter(); 
		controller.removeEntity(this);
		body.destroyFixture(fixture);
	}
	
	/**
	 * ferma il timer della bomba
	 */
	public void freezeTimer() {
		bTimerFreezed = true;
	}
	
	/**
	 * fa ripartire il timer della bomba
	 */
	public void restartTimer() {
		timer = TIMER_DURATION;
		bTimerFreezed = false;
	}
	
	/**
	 * metodo get per il campo owner
	 * @return il valore del campo owner
	 */
	public BombDropper getOwner() {
		return owner;
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Body getBody() {
		return body;
	}
	
	@Override
	public void draw(Batch batch) {
		animation.getSprite().draw(batch);
	}

	@Override
	public void collide(Entity e) {
		//if(e instanceof Explosion) explode();
	}
	
}
