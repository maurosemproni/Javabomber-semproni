package it.uniroma1.metodologie2018.javabomber.entities;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.entities.Explosion.ExplosionTile;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

/**
 * Classe che costruisce l'esplosione a partire dalle sue componenti
 * @author Mauro
 *
 */
public class ExplosionsManager {

	/**
	 * riferimento al Worldcontroller
	 */
	private WorldController controller;
	/**
	 * TextureAtlas da usare
	 */
	private TextureAtlas atlas;
	
	/**
	 * Costruttore della classe
	 * @param controller riferimento al WorldController
	 */
	public ExplosionsManager(WorldController controller) {
		this.controller = controller;
		atlas = new TextureAtlas(Gdx.files.internal("javabomber_texture_pack.pack"));
	}
	/**
	 * Costruttore da usare nel caso si voglia specificare un TextureAtlas diverso da quello di default
	 * @param controller riferimento al WorldController
	 * @param atlas il TextureAtlas da usare
	 */
	public ExplosionsManager(WorldController controller, TextureAtlas atlas) {
		this.controller = controller;
		this.atlas = atlas;
	}

	/**
	 * Metodo che costruisce l'entit� esplosione usando il suo builder
	 * @param pos la posizione dell'entit�
	 * @param state lo stato iniziale dell'entit�
	 * @param tile la parte che compone l'esplosione finale
	 * @return l'esplosione 
	 */
	private Explosion buildExplosion(Vector2 pos, AnimationState state, ExplosionTile tile) {
		return (new Explosion.ExplosionBuilder())
				.setWorldController(controller)
				.setPosition(pos)
				.setAnimationState(state)
				.setTile(tile)
				.setTextureAtlas(atlas)
				.build();
	}
	
	/**
	 * Viene richiamato dalla bomba al momento dell'esplosione
	 * @param controller riferimento al WorldController
	 * @param pos la posizione in cui deve apparire l'esplosione
	 * @param extent l'estensione dell'esplosione
	 */
	public void boom(WorldController controller, Vector2 pos, int extent) {
		List<Entity> list = new ArrayList<Entity>();
		list.add(buildExplosion(pos, AnimationState.EXPLOSION_CENTER, ExplosionTile.CENTER));
		Vector2 currentPos = new Vector2(0,0);
		boolean bGoRight = true;
		boolean bGoLeft = true;
		boolean bGoUp = true;
		boolean bGoDown = true;
		for(int i = 1; i <= extent; i++) {
			if(i == extent) {
				currentPos.set(pos.x + JavaBomber.TILE_SIZE * i, pos.y);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoRight)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_RIGHT, ExplosionTile.EDGE));
				
				currentPos.set(pos.x - JavaBomber.TILE_SIZE * i, pos.y);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoLeft)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_LEFT, ExplosionTile.EDGE));
				
				currentPos.set(pos.x, pos.y + JavaBomber.TILE_SIZE * i);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoUp)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_TOP, ExplosionTile.EDGE));
				
				currentPos.set(pos.x, pos.y - JavaBomber.TILE_SIZE * i);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoDown)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_BOTTOM, ExplosionTile.EDGE));
			}
			else {
				currentPos.set(pos.x + JavaBomber.TILE_SIZE * i, pos.y);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoRight)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_MID_H, ExplosionTile.LEFT));
				else bGoRight = false;
				
				currentPos.set(pos.x - JavaBomber.TILE_SIZE * i, pos.y);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoLeft)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_MID_H, ExplosionTile.RIGHT));
				else bGoLeft = false;
				
				currentPos.set(pos.x, pos.y + JavaBomber.TILE_SIZE * i);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoUp)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_MID_V, ExplosionTile.UP));
				else bGoUp = false;
				
				currentPos.set(pos.x, pos.y - JavaBomber.TILE_SIZE * i);
				if(controller.getLevel().getMap().getBlock(currentPos) == null && bGoDown)
					list.add(buildExplosion(currentPos, AnimationState.EXPLOSION_MID_V, ExplosionTile.DOWN));
				else bGoDown = false;
			}
		}
		for(Entity e : list) controller.addEntity(e);
	}
}
