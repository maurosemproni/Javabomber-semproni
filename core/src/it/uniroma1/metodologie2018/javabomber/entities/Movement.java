package it.uniroma1.metodologie2018.javabomber.entities;

import it.uniroma1.metodologie2018.javabomber.entities.PlayerControlledMovement.Direction;

/**
 * Interfaccia che definisce i movimenti possibili per le entit�
 * @author Mauro
 *
 */
public interface Movement {

	/**
	 * effettua il movimento vero e proprio
	 */
	void move();
	
	/**
	 * nel caso in cui il movimento coinvolga un qualche algoritmo di intelligenza artificiale, quell'algoritmo deve essere richiamato da questo metodo
	 */
	void think();
	
	/**
	 * Definisce la nuova direzione da prendere
	 */
	void changeDirection();
	
	/**
	 * Aumenta la velocit� del movimento
	 */
	void SpeedUp();
	
	/**
	 * Diminuisce la velocit� del movimento
	 */
	void SpeedDown();
	
	/**
	 * restituisce la direzione corrente
	 * @return la direzione corrente
	 */
	Direction getDirection();
	
	/**
	 * restituisce la velocit� corrente
	 * @return la velocit� corrente
	 */
	float getSpeed();
}
