package it.uniroma1.metodologie2018.javabomber.entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.PlayerControlledMovement.Direction;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

/**
 * Un movimento random che cambia direzione ogni volta che avviene una collisione
 * @author Mauro
 *
 */
public class RandomMovement implements Movement {
	/**
	 * la velocit� del movimento
	 */
	public static final float SPEED = 32.0f;
	/**
	 * la velocit�
	 */
	private float speed;
	/**
	 * la direzione del moviemnto
	 */
	private Direction direction;
	/**
	 * il body al quale agganciare il movimento
	 */
	private Body body;
	/**
	 * riferimento al WorldController
	 */
	private WorldController controller;
	
	/**
	 * Costruttore della calsse
	 * @param controller riferimento al WorldController
	 * @param body body al quale agganciare il movimento
	 */
	public RandomMovement(WorldController controller, Body body) {
		this.controller = controller;
		this.body = body;
		this.direction = Direction.UP;
		speed = SPEED; 
	}
	
	@Override
	public void move() {
		body.setLinearVelocity(direction.getDirVector().x * speed, direction.getDirVector().y * speed);
		
	}
	
	@Override
	public void changeDirection() {
		Vector2 pos = body.getPosition();
		Entity blocco;
		Direction newDir;
		Random rand = new Random();
		int val = rand.nextInt(4);
		switch(val) {
		case 0: blocco = controller.getLevel().getMap().getBlock(new Vector2(pos.x, pos.y - JavaBomber.TILE_SIZE)); newDir = Direction.DOWN; break;
		case 1: blocco = controller.getLevel().getMap().getBlock(new Vector2(pos.x + JavaBomber.TILE_SIZE, pos.y)); newDir = Direction.RIGHT; break;
		case 2: blocco = controller.getLevel().getMap().getBlock(new Vector2(pos.x - JavaBomber.TILE_SIZE, pos.y)); newDir = Direction.LEFT; break;
		case 3: blocco = controller.getLevel().getMap().getBlock(new Vector2(pos.x, pos.y + JavaBomber.TILE_SIZE)); newDir = Direction.UP; break;
		default: blocco = controller.getLevel().getMap().getBlock(new Vector2(pos.x, pos.y + JavaBomber.TILE_SIZE)); newDir = Direction.UP; break;
		}
		if(blocco == null) {
			centerToTile();
			this.direction = newDir;
		}
		else changeDirection();
	}
	
	/**
	 * Posiziona l'entit� al centro del tile in cui si trova
	 */
	private void centerToTile() {
		Vector2 result = new Vector2();
		float deltaX = (body.getPosition().x - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		float deltaY = (body.getPosition().y - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		result.x = deltaX < 16 ? body.getPosition().x - deltaX : body.getPosition().x + JavaBomber.TILE_SIZE - deltaX;
		result.y = deltaY < 16 ? body.getPosition().y - deltaY : body.getPosition().y + JavaBomber.TILE_SIZE - deltaY;
		
		Gdx.app.postRunnable(new Runnable() {

			@Override
			public void run() {
				body.setTransform(result, 0);
				
			}
		});
	}

	@Override
	public void SpeedUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void SpeedDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Direction getDirection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public float getSpeed() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void think() {
		// TODO Auto-generated method stub
		
	}

}