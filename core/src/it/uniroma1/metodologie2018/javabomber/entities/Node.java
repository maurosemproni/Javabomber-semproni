package it.uniroma1.metodologie2018.javabomber.entities;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Definisce i nodi del grafo che rappresenta la mappa
 * @author Mauro
 *
 */
public class Node {
	
	/**
	 * I possibili stati in cui i nodi del grafo si possono trovare.
	 * @author Mauro
	 *
	 */
	public enum NodeStatus {
		EMPTY, BRICK_WALL, BOMB_TRAIL, ENEMY_PATH;	
	}
	
	/**
	 * Coordinata x del nodo sulla mappa
	 */
	private int x;
	/**
	 * Coordinata y del nodo sulla mappa
	 */
	private int y;
	/**
	 * lo status del nodo. Contiene informazioni riguardo l'entit� che occupa questo nodo
	 */
	private NodeStatus status;
	/**
	 * il cammino pi� breve per raggiungere questo nodo da un eventuale nodo sorgente
	 */
	private List<Node> shortestPath = new LinkedList<>();
	/**
	 * la distanza che separa questo nodo da un eventuale nodo sorgente
	 */
	private Integer distance = Integer.MAX_VALUE;
	/**
	 * i nodi adiacenti a questo
	 */
	Map<Node, Integer> adjacentNodes = new HashMap<>();
	
	/**
	 * Costruttore della classe
	 * @param x la coordinata x
	 * @param y la coordinata y
	 * @param status l'entit� che occupa questo nodo (EMPTY se nessuna)
	 */
	public Node(int x, int y, NodeStatus status) {
		this.x = x;
		this.y = y;
		this.status = status;
	}
	
	/**
	 * metodo get per il campo x
	 * @return la coordinata x
	 */
	public int getX() { return x; }
	
	/**
	 * metodo get per il campo y
	 * @return la coordinata y
	 */
	public int getY() { return y; }
	
	/**
	 * metodo get per il campo status
	 * @return il valore attuale del campo status
	 */
	public NodeStatus getStatus() { return status; }
	
	/**
	 * metodo get per il campo distance
	 * @return la distanza da questo nodo
	 */
	public int getDistance() { return distance; }
	
	/**
	 * metodo get per il campo shortestPath
	 * @return il percorso pi� veloce per raggiungere questo nodo
	 */
	public List<Node> getShortestPath() { return shortestPath; }
	
	/**
	 * metodo get per il campo adjacentNodes
	 * @return la mappa contenente tutti i nodi adiacenti a questo
	 */
	public Map<Node, Integer> getAdjacentNodes() { return adjacentNodes; }
	
	/**
	 * metodo set per il campo status
	 * @param status il nuovo status
	 */
	public void setStatus(NodeStatus status) { this.status = status; }
	
	/**
	 * metodo set per il campo distance
	 * @param distance la nuova distanza
	 */
	public void setDistance(int distance) { this.distance = distance; }
	
	/**
	 * metodo set per il campo shortestPath
	 * @param shortestPath il nuovo percorso pi� veloce
	 */
	public void setShortestPath(List<Node> shortestPath) { this.shortestPath = shortestPath; }
	
	/**
	 * Aggiunge il nodo alla mappa dei nodi adiacenti, calcolando la distanza. 
	 * Se il nodo contiene un muro distruttibile la distanza � data dal valore arbitrario 20,
	 * altrimenti � uguale a 1
	 * @param node il nodo da aggiungere
	 */
	public void addToAdjacentsNodes(Node node) { 
		int distance = node.status == NodeStatus.BRICK_WALL ? 20 : 1;
		this.adjacentNodes.put(node, distance); 
		//System.out.println("node: " + node + " added to node: " + this + " list");
	}
	
	@Override
	public String toString() {
		return "[" + x + "," + y + "," + status.toString() + "]";
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == null) return false;
		if(!(o instanceof Node)) return false;
		Node node = (Node) o;
		return node.x == x && node.y == y;
	}
	
	@Override
	public int hashCode() {
	    return Objects.hash(x, y);
	}
}
