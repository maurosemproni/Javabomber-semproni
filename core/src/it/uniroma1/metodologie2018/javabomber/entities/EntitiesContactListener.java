package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * Contact listener per la gestione delle collisioni
 * @author Mauro
 *
 */
public class EntitiesContactListener implements ContactListener {

	@Override
	public void beginContact(Contact contact) {
		Entity e1 = (Entity) contact.getFixtureA().getUserData();
		Entity e2 = (Entity) contact.getFixtureB().getUserData();
		if(e1 != null && e2 != null) {
			e1.collide(e2);
			e2.collide(e1);
		}
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

}
