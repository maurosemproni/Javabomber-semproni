package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.GraphicEntity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

/**
 * Classe che definisce tutte le entit� di gioco: personaggi giocabili, nemici, bombe, power-up...
 * @author Mauro
 *
 */
public class GameEntity extends Animated implements GraphicEntity {
	
	
	/**
	 * Builder per la Classe GameEntity. 
	 * @author Mauro
	 *
	 * @param <T> il builder della sottoclasse
	 */
	public static abstract class GameEntityBuilder<T extends GameEntityBuilder<T>>{
		/**
		 * posizione dell'entit�
		 */
		protected Vector2 pos;
		/**
		 * riferimento al WorldController
		 */
		protected WorldController controller;
		/**
		 * TextureAtlas in cui � presente lo sprite della Entity
		 */
		protected TextureAtlas atlas;
		/**
		 * Nome della Region contenuta del TextureAtlas
		 */
		protected String regionName;
		
		/**
		 * Permette di chiamare in cascata i metodi set di questo builder insieme ai metodi set specifici delle sottoclassi
		 * @return un oggetto del tipo del Builder della classe pi� specifica
		 */
		protected abstract T self();
		
		/**
		 * metodo set per il campo controller
		 * @param controller il riferimoento al WorldController
		 * @return un oggetto builder della calsse pi� specifica
		 */
		public T setWorldController(WorldController controller) {
			this.controller = controller;
			return self();
		}
		
		/**
		 * metodo set per il campo pos
		 * @param pos la posizione iniziale
		 * @return un oggetto builder della calsse pi� specifica
		 */
		public T setPosition(Vector2 pos) {
			this.pos = pos;
			return self();
		}
		
		/**
		 * metodo set per il campo atlas
		 * @param atlas il TextureAltas da usare
		 * @return un oggetto builder della calsse pi� specifica
		 */
		public T setTextureAtlas(TextureAtlas atlas) {
			this.atlas = atlas;
			return self();
		}
		
		/**
		 * metodo set per il campo regionName
		 * @param regionName il nome della region
		 * @return un oggetto builder della calsse pi� specifica
		 */
		public T setRegionName(String regionName) {
			this.regionName = regionName;
			return self();
		}
		
		/**
		 * costruisce l'oggetto con i parametri selezionati
		 * @return una GameEntity
		 */
		public GameEntity build() {
			GameEntity instance = new GameEntity();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.atlas = this.atlas;
			instance.regionName = this.regionName;
			return instance;
		}
	}
	
	/**
	 * posizione dell'entit�
	 */
	protected Vector2 pos;
	/**
	 * riferimento al WorldController
	 */
	protected WorldController controller;
	/**
	 * TextureAtlas in cui � presente lo sprite della Entity
	 */
	protected TextureAtlas atlas;
	/**
	 * Nome della Region contenuta del TextureAtlas
	 */
	protected String regionName;
	/**
	 * Body dell'entit�, necessario per rilevare e gestire le collisioni
	 */
	protected Body body;
	/**
	 * la fixture dell'oggetto
	 */
	protected Fixture fixture;
	
	/**
	 * metodo che verr� usato dalle classi sottostanti per definire Body, Fixture, Shape dell'entit�
	 */
	public void defineEntity() {
		
	}
	
	/**
	 * cambia il categoryBit dell'entit� a seconda degli oggetti con i quali si vuole che collida
	 * @param filterBit il nuovo filterBit
	 */
	public void setCategoryFilter(short filterBit) {
		Filter filter = new Filter();
		filter.categoryBits = filterBit;
		fixture.setFilterData(filter);
	}
	
	/**
	 * Azione principale del personaggio (sparare, lasciare bombe...)
	 */
	public void mainAction() {
		
	}

	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Vector2 getPosition() {
		return pos;
	}
	
	/**
	 * restituisce la posizione pensando alla mappa come una matrice di tiles
	 * @return la posizione del tile occupato
	 */
	public Vector2 getTilePos() {
		int tileX = (int)Math.ceil(pos.x / JavaBomber.TILE_SIZE);
		int tileY = (int)Math.ceil(pos.y / JavaBomber.TILE_SIZE);
		return new Vector2(tileX, tileY);
	}

	@Override
	public void draw(Batch batch) {
		//sprite.draw(batch);
	}
	
	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void collide(Entity e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int getZindex() {
		return Config.GAME_ENTITY_Z_INDEX;
	}

}
