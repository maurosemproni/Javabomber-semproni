package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;

/**
 * Decorator che crea le animazioni base delle entit� a partire da un TextureAtlas
 * @author Mauro
 *
 */
public class DecoratorAnimatedEntity extends AnimatedDecorator{

	/**
	 * TextureAtlas da usare per creare le animazioni
	 */
	private TextureAtlas ATLAS;
	
	/**
	 * Enum che elenca tutte le possibili animazioni base delle entit�
	 * @author Mauro
	 *
	 */
	public enum AnimationState {
		//Per i personaggi
		STANDING("bomberman_front"), 
		GOING_UP("bomberman_back"), 
		GOING_DOWN("bomberman_front"), 
		GOING_LEFT("bomberman_left"), 
		GOING_RIGHT("bomberman_right"), 
		DYING("bomberman_front"), // DA SOSTITUIRE NON APPENA PRONTA L'ANIMAZIONE DYING
		//Per la Bomba
		BOMB_PLACED("bomb"),
		BOMB_PULSE("bomb"),
		//Per l'esplosione
		EXPLOSION_START("bomb"),
		EXPLOSION_BOTTOM("expl_bottom"),
		EXPLOSION_CENTER("expl_center"),
		EXPLOSION_TOP("expl_top"),
		EXPLOSION_LEFT("expl_left"),
		EXPLOSION_RIGHT("expl_right"),
		EXPLOSION_MID_H("expl_mid_h"),
		EXPLOSION_MID_V("expl_mid_v");
		
		/**
		 * il nome della region da cercare all'interno del TextureAtlas
		 */
		private String regionName;
		
		/**
		 * Costruttore della enum
		 * @param regionName il nome della region
		 */
		AnimationState(String regionName) {
			this.regionName = regionName;
		}
		/**
		 * metodo get per il campo regionName
		 * @return il valore attuale del parametro regionName
		 */
		public String getRegionName() {
			return regionName;
		}
		
	}
	
	/**
	 * L'enti� da animare
	 */
	private GameEntity animated;
	/**
	 * lo stato corrente dell'animazione
	 */
	private AnimationState currentState;
	/**
	 * lo stato dell'animazione che precede currentState
	 */
	private AnimationState previousState;
	/**
	 * timer che definisce la durata di ogni frame dell'animazione
	 */
	private float stateTimer;
	/**
	 * animazione per camminare verso l'alto
	 */
	private Animation<TextureRegion> bombermanGoUp;
	/**
	 * animazione per camminare verso il basso
	 */
	private Animation<TextureRegion> bombermanGoDown;
	/**
	 * animazione per camminare verso sinistra
	 */
	private Animation<TextureRegion> bombermanGoLeft;
	/**
	 * animazione per camminare verso destra
	 */
	private Animation<TextureRegion> bombermanGoRight;
	/**
	 * animazione per la morte
	 */
	private Animation<TextureRegion> bombermanDies; // da implementare non appena disponibile l'animazione DYING
	/**
	 * animazione per la bomba
	 */
	private Animation<TextureRegion> bomb;
	/**
	 * animazione per la parte centrale dell'esplosione
	 */
	private Animation<TextureRegion> explosionCenter;
	/**
	 * animazione per il bordo superiore dell'esplosione
	 */
	private Animation<TextureRegion> explosionTop;
	/**
	 * animazione per il bordo inferiore dell'esplosione
	 */
	private Animation<TextureRegion> explosionBottom;
	/**
	 * animazione per il bordo sinistro dell'esplosione
	 */
	private Animation<TextureRegion> explosionLeft;
	/**
	 * animazione per il bordo destro dell'esplosione
	 */
	private Animation<TextureRegion> explosionRight;
	/**
	 * animazione per il ramo intermendio orizzontale dell'esplosione
	 */
	private Animation<TextureRegion> explosionMiddleHoriz;
	/**
	 * animazione per il ramo intermendio verticale dell'esplosione
	 */
	private Animation<TextureRegion> explosionMiddleVert;

	/**
	 * Costruttore del Decorator
	 * @param animated l'entit� da animare
	 * @param initialState lo stato iniziale dell'animazione
	 * @param ATLAS il TextureAtlas da usare
	 */
	public DecoratorAnimatedEntity(GameEntity animated, AnimationState initialState, TextureAtlas ATLAS) {
		super(animated);
		this.animated = animated;
		this.ATLAS = ATLAS;
		
		currentState = initialState;
		previousState = initialState;
		stateTimer = 0;
		
		int spriteHeight = JavaBomber.TILE_SIZE + JavaBomber.TILE_SIZE / 2;
		if(animated instanceof Terminator) spriteHeight = JavaBomber.TILE_SIZE * 2;
		if(animated instanceof Bomb || animated instanceof Explosion) spriteHeight = JavaBomber.TILE_SIZE;
	
		bombermanGoUp = defineAnimation(AnimationState.GOING_UP, 3, JavaBomber.TILE_SIZE, spriteHeight);
		bombermanGoDown = defineAnimation(AnimationState.GOING_DOWN, 3, JavaBomber.TILE_SIZE, spriteHeight);
		bombermanGoLeft = defineAnimation(AnimationState.GOING_LEFT, 3, JavaBomber.TILE_SIZE, spriteHeight);
		bombermanGoRight = defineAnimation(AnimationState.GOING_RIGHT, 3, JavaBomber.TILE_SIZE, spriteHeight);
		bomb = defineAnimation(AnimationState.BOMB_PULSE, 3, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionCenter = defineAnimation(AnimationState.EXPLOSION_CENTER, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionTop = defineAnimation(AnimationState.EXPLOSION_TOP, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionBottom = defineAnimation(AnimationState.EXPLOSION_BOTTOM, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionLeft = defineAnimation(AnimationState.EXPLOSION_LEFT, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionRight = defineAnimation(AnimationState.EXPLOSION_RIGHT, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionMiddleHoriz = defineAnimation(AnimationState.EXPLOSION_MID_H, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		explosionMiddleVert = defineAnimation(AnimationState.EXPLOSION_MID_V, 4, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);

		sprite = new Sprite(getRegion(currentState), JavaBomber.TILE_SIZE, spriteHeight, JavaBomber.TILE_SIZE, spriteHeight);
		sprite.setOrigin(sprite.getWidth()/2.f, sprite.getHeight()/2.f);
		sprite.setPosition(animated.getPosition().x, animated.getPosition().y);
		sprite.setRegion(getRegion(currentState));
	}

	@Override
	public void animate(float deltaTime) {
		sprite.setRegion(getFrame(deltaTime));
	}
	
	/**
	 * restituisce il frame corrente dell'animazione 
	 * @param deltaTime il deltatime che viene passato al metodo update
	 * @return il frame corrente dell'animazione
	 */
	private TextureRegion getFrame(float deltaTime) {
		currentState = checkCurrentState();
		TextureRegion newRegion;
		switch(currentState) {
			case BOMB_PLACED: newRegion = bomb.getKeyFrame(stateTimer, true); break;
			case BOMB_PULSE: newRegion = bomb.getKeyFrame(stateTimer, true); break;
			case EXPLOSION_CENTER: newRegion = explosionCenter.getKeyFrame(stateTimer); break;
			case EXPLOSION_TOP: newRegion = explosionTop.getKeyFrame(stateTimer); break;
			case EXPLOSION_BOTTOM: newRegion = explosionBottom.getKeyFrame(stateTimer); break;
			case EXPLOSION_LEFT: newRegion = explosionLeft.getKeyFrame(stateTimer); break;
			case EXPLOSION_RIGHT: newRegion = explosionRight.getKeyFrame(stateTimer); break;
			case EXPLOSION_MID_H: newRegion = explosionMiddleHoriz.getKeyFrame(stateTimer); break;
			case EXPLOSION_MID_V: newRegion = explosionMiddleVert.getKeyFrame(stateTimer); break;
			case GOING_RIGHT: newRegion = bombermanGoRight.getKeyFrame(stateTimer, true); break;
			case GOING_LEFT: newRegion = bombermanGoLeft.getKeyFrame(stateTimer, true); break;
			case GOING_UP: newRegion = bombermanGoUp.getKeyFrame(stateTimer, true); break;
			case GOING_DOWN: newRegion = bombermanGoDown.getKeyFrame(stateTimer, true); break;
			case STANDING:
			default:
				newRegion = getRegion(AnimationState.STANDING); break;
		}
		stateTimer = currentState == previousState ? stateTimer + deltaTime : 0;
		previousState = currentState;
		return newRegion;
	}
	
	/**
	 * Controlla lo stato attuale dell'animazione in relazione all'entit� da animare
	 * @return lo stato attuale dell'animazione
	 */
	private AnimationState checkCurrentState() {
		if(animated instanceof Bomb) return AnimationState.BOMB_PULSE;
		if(animated instanceof Explosion) return currentState; 
		if(animated.getBody().getLinearVelocity().x > 0) return AnimationState.GOING_RIGHT;
		if(animated.getBody().getLinearVelocity().x < 0) return AnimationState.GOING_LEFT;
		if(animated.getBody().getLinearVelocity().y > 0) return AnimationState.GOING_UP;
		if(animated.getBody().getLinearVelocity().y < 0) return AnimationState.GOING_DOWN;
		else return AnimationState.STANDING;
	}
	
	/**
	 * Costruisce l'animazione 
	 * @param state lo stato iniziale
	 * @param framesCount il numero di frame di cui � coposta l'animazione
	 * @param frameWidth la larghezza di ogni frame
	 * @param frameHeight l'altezza di ogni frame
	 * @return l'animazione completa
	 */
	private Animation<TextureRegion> defineAnimation(AnimationState state, int framesCount, int frameWidth, int frameHeight) {
		Array<TextureRegion> frames = new Array<>();
		for(int i = 0; i < framesCount; i++)
			try {
				frames.add(new TextureRegion(getRegion(state), i * frameWidth, 0, frameWidth, frameHeight));
			}
			catch (java.lang.NullPointerException e) {

			}
		return new Animation<TextureRegion>(0.1f, frames);
	}
	
	/**
	 * Controlla se l'animazione corrente sia finita
	 * @return true se l'animazione � terminata, false altrimenti
	 */
	public boolean isCurrentAnimationEnded() {
		return 	explosionCenter.isAnimationFinished(stateTimer)||
				explosionTop.isAnimationFinished(stateTimer)||
				explosionBottom.isAnimationFinished(stateTimer)||
				explosionLeft.isAnimationFinished(stateTimer)||
				explosionRight.isAnimationFinished(stateTimer)||
				explosionMiddleHoriz.isAnimationFinished(stateTimer)||
				explosionMiddleVert.isAnimationFinished(stateTimer);
	}
	
	/**
	 * Metodo ausiliario necessario alla costruzione dell'animazione. restituisce la regione delle TextureAtlas a partire dallo stato dell'animazione
	 * @param state lo stato dell'animazione
	 * @return la TextureRegion corrispondente
	 */
	private TextureRegion getRegion(AnimationState state) {
		int spriteHeight = JavaBomber.TILE_SIZE + JavaBomber.TILE_SIZE / 2;
		if(animated instanceof Terminator) spriteHeight = JavaBomber.TILE_SIZE * 2;
		switch(state) {
		case STANDING:			return new TextureRegion(ATLAS.findRegion(state.getRegionName()), JavaBomber.TILE_SIZE, 0, JavaBomber.TILE_SIZE, spriteHeight);
		case BOMB_PLACED:		return new TextureRegion(ATLAS.findRegion(state.getRegionName()), JavaBomber.TILE_SIZE, 0, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		case EXPLOSION_START:	return new TextureRegion(ATLAS.findRegion(state.getRegionName()), JavaBomber.TILE_SIZE * 3, 0, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		default: 				return new TextureRegion(ATLAS.findRegion(state.getRegionName()));
		}
	}

}
