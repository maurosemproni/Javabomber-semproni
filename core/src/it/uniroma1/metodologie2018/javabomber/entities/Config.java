package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldRenderer;
import it.uniroma1.metodologie2018.javabomber.screens.PlayScreen;

public class Config {
	
	public static final TextureAtlas BOMBERMAN_ATLAS = new TextureAtlas(Gdx.files.internal("javabomber_texture_pack.pack"));
	public static final TextureAtlas ARMOR_ENEMY_ATLAS = new TextureAtlas(Gdx.files.internal("enemy_armor.pack"));
	public static final TextureAtlas POWER_UP_PACK = new TextureAtlas(Gdx.files.internal("power_up.pack"));
	public static final TextureAtlas BOSS_ATLAS = new TextureAtlas(Gdx.files.internal("boss.pack"));
	
	public static final int GAME_ENTITY_Z_INDEX = 10;
	public static final int BLOCK_Z_INDEX = 10;
	public static final int EXPLOSION_Z_INDEX = 7;
	public static final int POWER_UP_Z_INDEX = 5;
	
	private static JavaBomber game;
	private static WorldRenderer renderer;
	private static PlayScreen screen;
	
	public static void setGame(JavaBomber game) { Config.game = game; }
	public static JavaBomber getGame() { return game; }
	public static void setWorldRenderer(WorldRenderer renderer) { Config.renderer = renderer;}
	public static WorldRenderer getWorldRenderer() { return renderer; }
	public static void setScreen(PlayScreen screen) { Config.screen = screen;}
	public static PlayScreen getScreen() { return screen; }
}
