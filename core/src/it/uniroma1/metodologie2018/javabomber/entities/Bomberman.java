package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.interfaces.BombDropper;
import it.uniroma1.metodologie2018.javabomber.interfaces.Enemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.powerup.BombDown;
import it.uniroma1.metodologie2018.javabomber.powerup.BombUp;
import it.uniroma1.metodologie2018.javabomber.powerup.ExplosionDown;
import it.uniroma1.metodologie2018.javabomber.powerup.ExplosionUp;
import it.uniroma1.metodologie2018.javabomber.powerup.LifeUp;
import it.uniroma1.metodologie2018.javabomber.powerup.SpeedDown;
import it.uniroma1.metodologie2018.javabomber.powerup.SpeedUp;

/**
 * Definisce il personaggio principale Bomberman
 * @author Mauro
 *
 */
public class Bomberman extends GameEntity implements BombDropper {
	
	/**
	 * Classe builder per l'oggetto Bomberman
	 * @author Mauro
	 *
	 */
	public static class BombermanBuilder extends GameEntityBuilder<BombermanBuilder>{
		
		/**
		 * il numero delle vite di Bomberman
		 */
		private int lives;
		
		@Override
		protected BombermanBuilder self() {
			return this;
		}
		
		/**
		 * metodo set per il campo lives
		 * @param lives il numero delle vite iniziali che si vogliono assegnare a Bomberman
		 * @return un oggetto di tipo BombermanBuilder
		 */
		public BombermanBuilder setLives(int lives) {
			this.lives = lives;
			return this;
		}
		
		/**
		 * metodo che costruisce l'oggetto di tipo bomberman con l ecaratteristiche scelte
		 * @return un nuovo oggetto di tipo Bomberman
		 */
		public Bomberman build() {
			Bomberman instance = new Bomberman();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.animation = new DecoratorAnimatedEntity(instance, AnimationState.STANDING, Config.BOMBERMAN_ATLAS);
			instance.lives = this.lives;
			instance.explosionExtent = 1;
			instance.bombStock = 1;
			instance.bombCounter = 0;
			instance.defineEntity();
			instance.movement = new PlayerControlledMovement(instance);
			return instance;
		}

	}
	
	/**
	 * Il decorator usato per l'animazione
	 */
	private DecoratorAnimatedEntity animation;
	/**
	 * oggetto che definisce il movimento
	 */
	private Movement movement;
	
	/**
	 * le vite del personaggio
	 */
	private int lives;
	/**
	 * l'estensione dell'esplosione che � in grado di generare
	 */
	private int explosionExtent;
	/**
	 * numero massimo di bombe a disposizione
	 */
	private int bombStock;
	/**
	 * Numero di bombe che sono state piazzate ma non sono ancora esplose
	 */
	private int bombCounter;
	/**
	 * indica se Bomberman � in grado o meno di calciare le bombe
	 */
	private boolean bCanKickBombs = false;
	
	
	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + animation.getSprite().getWidth() / 2, pos.y + animation.getSprite().getHeight() / 2);
		bdef.type = BodyDef.BodyType.DynamicBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(animation.getSprite().getRegionWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getRegionHeight() / 3 - JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.PLAYER_BIT;
		fdef.filter.maskBits = JavaBomber.BOMB_BIT ^ JavaBomber.BLOCK_BIT ^ JavaBomber.EXPLOSION_BIT ^ JavaBomber.ENEMY_BIT ^ JavaBomber.POWER_UP_BIT ;
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		animation.animate(deltaTime);
		pos = body.getPosition();
		controller.getWorld().step(deltaTime, 6, 2);
		movement.move();
		animation.getSprite().setPosition(body.getPosition().x - animation.getSprite().getWidth() / 2, body.getPosition().y - animation.getSprite().getHeight() / 3);
	}
	
	/**
	 * metodo get per il campo movement
	 * @return il valore del campo movement
	 */
	public Movement getMovement() {
		return movement;
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void draw(Batch batch) {
		animation.getSprite().draw(batch);
	}

	@Override
	public void collide(Entity e) {
		if(e instanceof Explosion) die();
		if(e instanceof ExplosionUp) explosionExtent++;
		if(e instanceof ExplosionDown && explosionExtent > 1) explosionExtent--;
		if(e instanceof LifeUp) lives++;
		if(e instanceof SpeedUp) movement.SpeedUp();
		if(e instanceof SpeedDown) movement.SpeedDown();
		if(e instanceof BombUp) { bombStock++; }
		if(e instanceof BombDown && bombStock > 1) { bombStock--; if(bombCounter > bombStock) bombCounter = bombStock; }
		
		if(e instanceof Bomb && bCanKickBombs) e.getBody().setLinearVelocity(movement.getDirection().getDirVector().scl(movement.getSpeed() * 5));
		
		if(e instanceof Enemy) die();
	}
	
	@Override
	public void mainAction() {
		dropBomb();
	}
	
	@Override
	public void dropBomb() {
		// definisce la posizione in cui piazzare la bomba, ovvero al centro del tile pi� vicino al player
		if(bombCounter < bombStock) {
			bombCounter++;
			Vector2 bombPosition = centerToTile(pos.x - JavaBomber.TILE_SIZE / 2, pos.y - JavaBomber.TILE_SIZE / 2);
			Entity placed = (new Bomb.BombBuilder()).setWorldController(controller).setPosition(bombPosition).setOwner(this).build();
			this.controller.addEntity(placed);
		}
	}
	
	@Override
	public void restoreBombCounter() {
		if(bombCounter > 0) bombCounter--;
	}
	
	/**
	 * Calcola la posizione corrispondente al centro del tile sul quale si trova il personaggio
	 * @param x la posizione x del tile
	 * @param y la posizione y del tile
	 * @return la nuova posizione
	 */
	private Vector2 centerToTile(float x, float y) {
		Vector2 result = new Vector2();
		float deltaX = x % JavaBomber.TILE_SIZE;
		float deltaY = y % JavaBomber.TILE_SIZE;
		result.x = deltaX < 16 ? x - deltaX : x + JavaBomber.TILE_SIZE - deltaX;
		result.y = deltaY < 16 ? y - deltaY : y + JavaBomber.TILE_SIZE - deltaY;
		return result;
	}
	
	/**
	 * Definisce il coportamento del personaggio quando muore
	 */
	public void die() {
		controller.playerDead(--lives);
	}
	
	@Override
	public int getExplosionExtent() {
		return explosionExtent;
	}
	
	/**
	 * Aggiunge o toglie il bit della bomba dal filtro della fixture
	 */
	public void walkThroughBombsToggle() {
		Filter filter = fixture.getFilterData();
		filter.maskBits = (short)(fixture.getFilterData().maskBits ^ JavaBomber.BOMB_BIT);
		fixture.setFilterData(filter);
	}
	
	/**
	 * attiva/disattiva il power up che permette di calciare le bombe
	 */
	public void kickBombsToggle() {
		bCanKickBombs = !bCanKickBombs;
	}
	
	/**
	 * metodo getter per il campo lives
	 * @return il valore del campo lives
	 */
	public int getLives() {
		return lives;
	}
	
	/**
	 * Calcola la posizione del personaggio in relazione al tile che occupa
	 * @return il valore della poszione in relazione al tile
	 */
	public Vector2 getTilePos() {
		int tileX = (int)Math.floor(pos.x / JavaBomber.TILE_SIZE);
		int tileY = (int)Math.floor(pos.y / JavaBomber.TILE_SIZE);
		return new Vector2(tileX, tileY);
	}

}
