package it.uniroma1.metodologie2018.javabomber.entities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Node.NodeStatus;
import it.uniroma1.metodologie2018.javabomber.entities.PlayerControlledMovement.Direction;
import it.uniroma1.metodologie2018.javabomber.interfaces.Map;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.maps.BrickWall;

/**
 * Uno dei movimenti disponibili per i personaggi. L'entit� che implementa questo movimento trover� il percorso pi� rapido per raggiungere Bomberman e per ucciderlo piazzando una bomba
 * @author Mauro
 *
 */
public class TerminatorAI implements Movement {

	/**
	 * Costante che definisce la velocit� del movimento
	 */
	public static final float SPEED = 32.0f;
	/**
	 * il campo che contiene la velocit�
	 */
	private float speed;
	/**
	 * la direzione del movimento
	 */
	private Direction direction;
	/**
	 * il body a cui agganciare il movimento
	 */
	private Body body;
	/**
	 * riferimento al worldController
	 */
	private WorldController controller;
	/**
	 * Entit� alla quale � agganciato il movimento
	 */
	private GameEntity owner;
	/**
	 * Mappa sulla quale avviene il movimento
	 */
	private Map mappa;
	/**
	 * Contatore che determina quanto tempo nascondersi dopo aver piazato una bomba
	 */
	private int hideCounter = 0;
	
	/**
	 * Costruttore della classe
	 * @param controller riferimento al Worldcontroller
	 * @param owner riferimento all'entit� a cui � agganciato il movimento
	 * @param mappa mappa sulla quale si svolge il movimento
	 */
	public TerminatorAI(WorldController controller, GameEntity owner, Map mappa) {
		this.controller = controller;
		this.direction = Direction.UP;
		this.owner = owner;
		this.body = owner.body;
		this.mappa = mappa;
		speed = SPEED; 
	}
	
	@Override
	public void move() {
		body.setLinearVelocity(direction.getDirVector().x * speed, direction.getDirVector().y * speed);	
	}
	
	@Override
	public void think() {
		java.util.Map<Vector2,Node> graph = mapToGraph(); // ottengo un grafo a partire dalla mappa di gioco
		//printGraph(graph);
		Vector2 pos = toTilePos(owner.getPosition());
		graph = calculateShortestPath(graph, graph.get(pos)); // aggiorno il grafo con i percorsi pi� brevi per ogni nodo partendo dalla mia posizione
		if(graph.get(pos).getStatus() == NodeStatus.BOMB_TRAIL)
			direction = headTo(graph, pos, NodeStatus.EMPTY, NodeStatus.ENEMY_PATH); // mi dirigo verso un tile sicuro
		else if(graph.get(pos).getStatus() == NodeStatus.ENEMY_PATH)
			owner.mainAction(); // piazzo una bomba
		else 
			direction = headTo(graph, pos, NodeStatus.ENEMY_PATH);
		
	}
	
	/**
	 * Calcola il percorso da seguire e stavbilisce quale direzione prendere
	 * @param graph il grafo che rappresenta la mappa 
	 * @param pos la posizione iniziale dell'entit�
	 * @param destination gli status dei nodi che ci interessa raggiungere
	 * @return la direzione da prendere
	 */
	private Direction headTo(java.util.Map<Vector2,Node> graph, Vector2 pos, NodeStatus... destination) {
		Node closestNode = null;
		int minDistance = Integer.MAX_VALUE;
		for(java.util.Map.Entry<Vector2, Node> entry : graph.entrySet()) {
			Node value = entry.getValue();
			for(NodeStatus status :  destination) {
				if(value.getStatus() == status && value.getDistance() < minDistance) {
					minDistance = value.getDistance();
					closestNode = value;
				}
			}
		}

		if(closestNode == null) {
			centerToTile();
			return Direction.IDLE;
		}
		List<Node> path = closestNode.getShortestPath();
		path.add(closestNode);
		Node firstStep = path.get(1);
		
		/* ======DEBUG======
		System.out.println(pos);
		System.out.println(firstStep);
		System.out.println(direction);
		int i = 0;
		for(Node n : path) {
			System.out.println(i + ": " + path.get(i));
			i++;
		}
		System.out.println("====================");
		======DEBUG======*/
		
		Direction result = direction;
		if(firstStep.getStatus() == NodeStatus.BRICK_WALL) {
			owner.mainAction();
			centerToTile();
			return Direction.IDLE;
		}
		if((graph.get(pos).getStatus() == NodeStatus.EMPTY || graph.get(pos).getStatus() == NodeStatus.ENEMY_PATH) &&
		   (firstStep.getStatus() == NodeStatus.BOMB_TRAIL)) {
			result = Direction.IDLE;
			hideCounter = 60;		
		}
		
		else {
			if(hideCounter > 0) {
				hideCounter--;
			}
			else {
				if(firstStep.getX() > pos.x) result = Direction.RIGHT;
				else if(firstStep.getX() < pos.x) result = Direction.LEFT;
				else if(firstStep.getY() > pos.y) result = Direction.UP;
				else if(firstStep.getY() < pos.y) result = Direction.DOWN;
				else if(firstStep.getX() == pos.x && firstStep.getY() == (int)pos.y) result = direction;
			}
		}
		if(direction != result) centerToTile();
		return result;
	}
	
	/**
	 * Calcola il percorso pi� breve per ogni nodo del grafo
	 * @param graph il grafo su cui effettuare il calcolo
	 * @param source il nodo sorgente
	 * @return il grafo aggiornato
	 */
	public static java.util.Map<Vector2,Node> calculateShortestPath(java.util.Map<Vector2,Node> graph, Node source) {
	    source.setDistance(0);
	 
	    Set<Node> settledNodes = new HashSet<>();
	    Set<Node> unsettledNodes = new HashSet<>();
	 
	    unsettledNodes.add(source);
	 
	    while (unsettledNodes.size() != 0) {
	        Node currentNode = getLowestDistanceNode(unsettledNodes);
	        unsettledNodes.remove(currentNode);
	        for (Entry <Node, Integer> adjacencyPair: 
	          currentNode.getAdjacentNodes().entrySet()) {
	            Node adjacentNode = adjacencyPair.getKey();
	            Integer edgeWeight = adjacencyPair.getValue();
	            if (!settledNodes.contains(adjacentNode)) {
	                calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
	                unsettledNodes.add(adjacentNode);
	            }
	        }
	        settledNodes.add(currentNode);
	    }
	    return graph;
	}
	
	/**
	 * Trova il nodo a distanza minima
	 * @param unsettledNodes nodi non ancora visitati
	 * @return il nodo pi� vicino
	 */
	private static Node getLowestDistanceNode(Set <Node> unsettledNodes) {
	    Node lowestDistanceNode = null;
	    int lowestDistance = Integer.MAX_VALUE;
	    for (Node node: unsettledNodes) {
	        int nodeDistance = node.getDistance();
	        if (nodeDistance < lowestDistance) {
	            lowestDistance = nodeDistance;
	            lowestDistanceNode = node;
	        }
	    }
	    return lowestDistanceNode;
	}
	
	/**
	 * Calcola la distanza minima tra due nodi
	 * @param evaluationNode il nodo da valutare
	 * @param edgeWeigh la distanza minima attuale
	 * @param sourceNode il nodo sorgente
	 */
	private static void calculateMinimumDistance(Node evaluationNode, Integer edgeWeigh, Node sourceNode) {
	    Integer sourceDistance = sourceNode.getDistance();
	    if (sourceDistance + edgeWeigh < evaluationNode.getDistance()) {
	        evaluationNode.setDistance(sourceDistance + edgeWeigh);
	        LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
	        shortestPath.add(sourceNode);
	        evaluationNode.setShortestPath(shortestPath);
	    }
	}
	
	/**
	 * Crea un grafo a partire dalla mappa
	 * @return il grafo che rappresenta la mappa
	 */
	private java.util.Map<Vector2,Node> mapToGraph() {
		java.util.Map<Vector2,Node> graph = new HashMap<>();
		Vector2 currentPos = new Vector2();
		for(int i = 0; i < mappa.getSize().y; i++) {
			for(int j = 0; j < mappa.getSize().x; j++) {
				currentPos.x = j * JavaBomber.TILE_SIZE;
				currentPos.y = i * JavaBomber.TILE_SIZE;
				if(mappa.getBlock(currentPos) == null) graph.put(new Vector2(j, i), new Node(j, i, NodeStatus.EMPTY));
				else if(mappa.getBlock(currentPos) instanceof BrickWall) graph.put(new Vector2(j, i), new Node(j, i, NodeStatus.BRICK_WALL));
			}
		}
		java.util.Map<Vector2,Node> graphCopy = graph;
		Iterator<java.util.Map.Entry<Vector2,Node>> it = graphCopy.entrySet().iterator();
		while(it.hasNext()) {
			 java.util.Map.Entry<Vector2,Node> pair = it.next();
			 Node node1 = pair.getValue();
			 Iterator<java.util.Map.Entry<Vector2,Node>> it2 = graphCopy.entrySet().iterator();
			 while(it2.hasNext()) {
				 java.util.Map.Entry<Vector2,Node> pair2 = it2.next();
				 Node node2 = pair2.getValue();
				 if((node1.getX() == node2.getX() + 1 && node1.getY() == node2.getY()) ||
					(node1.getX() == node2.getX() - 1 && node1.getY() == node2.getY()) ||
					(node1.getY() == node2.getY() + 1 && node1.getX() == node2.getX()) ||
					(node1.getY() == node2.getY() - 1 && node1.getX() == node2.getX()) ){
						 node1.addToAdjacentsNodes(node2);
						 node2.addToAdjacentsNodes(node1);
				 }
			 }
		}

		// Calcolo possibili percorsi nemico
		Optional<Bomberman> target = controller.getEntities().stream().filter(e -> e instanceof Bomberman).map(e -> (Bomberman)e).findAny();
		drawTrail(graph, target.get().getTilePos(), NodeStatus.ENEMY_PATH, target.get().getTilePos(), new HashSet<Node>());
		
		// Calcolo delle bombe e relative linee di fuoco
		List<Vector2> bombsPlaced = controller.getEntities().stream().filter(e -> e instanceof Bomb).map(e -> ((Bomb)e).getTilePos()).collect(Collectors.toList());
		for(Vector2 bombPos : bombsPlaced) {
			drawTrail(graph, bombPos, NodeStatus.BOMB_TRAIL, bombPos, new HashSet<Node>());
		}
		
		return graph;
	}
	
	/**
	 * Aggiorna il grafo con le posizioni delle bombe, le relative aree di esplosione, il nemico, la relativa area di movimento
	 * @param graph il grafo che rappresenta la mappa
	 * @param start la posizione di partenza
	 * @param status lo status con cui aggiornare i nodi interessati
	 * @param coord le coordinate iniziali dell'entit� di cui aggiornare l'are di interesse
	 * @param nodesVisited i nodi gi� aggiornati
	 */
	private void drawTrail(java.util.Map<Vector2,Node> graph, Vector2 start, NodeStatus status, Vector2 coord, Set<Node> nodesVisited) {
		Node current = graph.get(start);
		if(current.getStatus() == NodeStatus.BRICK_WALL) return;
		else {
			current.setStatus(status);
			nodesVisited.add(current);
			//System.out.println(current.getAdjacentNodes().keySet().size());
			for(Node adjacent : current.getAdjacentNodes().keySet()) {
				if(!nodesVisited.contains(adjacent) && (adjacent.getX() == coord.x ^ adjacent.getY() == coord.y)) {
					Vector2 next = new Vector2(adjacent.getX(), adjacent.getY());
					drawTrail(graph, next, status, coord, nodesVisited);
				}
			}
		}
	}
	
	/**
	 * Posiziona l'entit� al centro del tile in cui si trova
	 */
	private void centerToTile() {
		Vector2 result = new Vector2();
		float deltaX = (body.getPosition().x - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		float deltaY = (body.getPosition().y - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		result.x = deltaX < 16 ? body.getPosition().x - deltaX : body.getPosition().x + JavaBomber.TILE_SIZE - deltaX;
		result.y = deltaY < 16 ? body.getPosition().y - deltaY : body.getPosition().y + JavaBomber.TILE_SIZE - deltaY;
		body.setTransform(result, 0);
	}
	
	/**
	 * Stampa un grafo. Solo per DEBUG
	 * @param graph il grafo da stampare
	 */
	private void printGraph(java.util.Map<Vector2,Node> graph) {
		Iterator<java.util.Map.Entry<Vector2,Node>> it = graph.entrySet().iterator();
		int[][] matrix = new int[20][20];
		while(it.hasNext()) {
			java.util.Map.Entry<Vector2,Node> pair = it.next();
			NodeStatus s = pair.getValue().getStatus();
			int value = 0;
			switch(s) {
			case EMPTY: value = 8; break;
			case BOMB_TRAIL: value = 3; break;
			case ENEMY_PATH: value = 4; break;
			case BRICK_WALL: value = 1; break;
			}
			matrix[pair.getValue().getX()][pair.getValue().getY()] = value;	 
		}
		for(int i = 0; i < matrix.length; i++) {
			for(int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[j][i]);
			}
			System.out.println();
		}
		System.out.println("=====================================");
	}
	
	/**
	 * Restituisce la posizione dell'entit� in relazione al tile sul quale si trova
	 * @param pos l aposizione in pixel
	 * @return la posizione relativa ai tiles
	 */
	private Vector2 toTilePos(Vector2 pos) {
		int tileX = (int)Math.floor(pos.x / JavaBomber.TILE_SIZE);
		int tileY = (int)Math.floor(pos.y / JavaBomber.TILE_SIZE);
		return new Vector2(tileX, tileY);
	}

	@Override
	public void SpeedUp() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void SpeedDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Direction getDirection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public float getSpeed() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void changeDirection() {
		// TODO Auto-generated method stub
		
	}

}
