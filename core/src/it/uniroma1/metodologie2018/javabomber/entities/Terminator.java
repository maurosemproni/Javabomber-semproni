package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.interfaces.BombDropper;
import it.uniroma1.metodologie2018.javabomber.interfaces.Enemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * Classe che definisce il Boss
 * @author Mauro
 *
 */
public class Terminator extends GameEntity implements Enemy, BombDropper {
	
/**
 * Classe annidata che definisce il builder per il Boss	
 * @author Mauro
 *
 */
public static class BossBuilder extends GameEntityBuilder<BossBuilder>{
		/**
		 * numero massimo di bombe a disposizione
		 */
		private int bombStock = 1;
		/**
		 * energia del Boss
		 */
		private int energy = 1;
		/**
		 * Descrive il movimento dell'entit�
		 */
		private Movement movement;
	
		@Override
		protected BossBuilder self() {
			return this;
		}
		
		/**
		 * metodo set per il cambobombStock
		 * @param bombStock il valore iniziale del parametro bombStock
		 * @return un oggetto di tipo BossBuilder
		 */
		public BossBuilder setBombStock(int bombStock) {
			this.bombStock = bombStock;
			return this;
		}
		
		/**
		 * metodo set per il campo energy
		 * @param energy l'energia iniziale
		 * @return un oggetto di tipo BossBuilder
		 */
		public BossBuilder setEnergy(int energy) {
			this.energy = energy;
			return this;
		}
		
		/**
		 * Metodo set per il campo movement
		 * @param movement il movimento che si intende assegnare a questa entit�
		 * @return this
		 */
		public BossBuilder setMovement(Movement movement) {
			this.movement = movement;
			return this;
		}
		
		/**
		 * metodo che costruisce l'oggetto Boss con i parametri indicati
		 */
		public Terminator build() {
			Terminator instance = new Terminator();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.explosionExtent = 10;
			instance.animation = new DecoratorAnimatedEntity(instance, AnimationState.STANDING, Config.BOSS_ATLAS);
			instance.defineEntity();
			if(movement == null)
				instance.movement = new TerminatorAI(controller, instance, controller.getLevel().getMap());
			else
				instance.movement = movement;
			instance.bombStock = this.bombStock;
			instance.energy = this.energy;
			return instance;
		}

	}

	/**
	 * decorator che definisce l'animazione del Boss
	 */
	private DecoratorAnimatedEntity animation;
	/**
	 * oggetto che definisce il movimento
	 */
	private Movement movement;
	/**
	 * definisce la grandezza dell'esplosione che le bombe piazzate da questo personaggio sono in grado di generare
	 */
	private int explosionExtent;
	/**
	 * numero massimo di bombe a disposizione
	 */
	private int bombStock;
	/**
	 * Numero di bombe che sono state piazzate ma non sono ancora esplose
	 */
	private int bombCounter;
	/**
	 * l'energia del personaggio
	 */
	private int energy;
	
	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + animation.getSprite().getWidth() / 2, pos.y + animation.getSprite().getHeight() / 2);
		bdef.type = BodyDef.BodyType.DynamicBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(animation.getSprite().getRegionWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getRegionHeight() / 4 - JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.ENEMY_BIT;
		fdef.filter.maskBits = JavaBomber.BOMB_BIT | JavaBomber.BLOCK_BIT | JavaBomber.EXPLOSION_BIT;
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	

	@Override
	public void draw(Batch batch) {
		animation.getSprite().draw(batch);
	}

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		animation.animate(deltaTime);
		pos = body.getPosition();
		controller.getWorld().step(deltaTime, 6, 2);
		movement.think();
		movement.move();
		animation.getSprite().setPosition(body.getPosition().x - animation.getSprite().getWidth() / 2, body.getPosition().y - animation.getSprite().getHeight() / 4);
	}

	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void collide(Entity e) {
		if(e instanceof Explosion) die();
	}
	
	@Override
	public void mainAction() {
		dropBomb();
	}
	
	@Override
	public void dropBomb() {
		if(bombCounter < bombStock) {
			bombCounter++;
			Vector2 bombPosition = centerToTile(pos.x - JavaBomber.TILE_SIZE / 2, pos.y - JavaBomber.TILE_SIZE / 2);
			Entity placed = (new Bomb.BombBuilder())
					.setWorldController(controller)
					.setPosition(bombPosition)
					.setOwner(this)
					.setTextureAtlas(new TextureAtlas(Gdx.files.internal("boss.pack")))
					.build();
			this.controller.addEntity(placed);
		}
	}
	/**
	 * Calcola la posizione corrispondente al centro del tile sul quale si trova il personaggio
	 * @param x la posizione x del tile
	 * @param y la posizione y del tile
	 * @return la nuova posizione
	 */
	private Vector2 centerToTile(float x, float y) {
		Vector2 result = new Vector2();
		float deltaX = x % JavaBomber.TILE_SIZE;
		float deltaY = y % JavaBomber.TILE_SIZE;
		result.x = deltaX < 16 ? x - deltaX : x + JavaBomber.TILE_SIZE - deltaX;
		result.y = deltaY < 16 ? y - deltaY : y + JavaBomber.TILE_SIZE - deltaY;
		return result;
	}
	
	@Override
	public int getExplosionExtent() {
		return explosionExtent;
	}
	
	@Override
	public void restoreBombCounter() {
		if(bombCounter > 0) bombCounter--;
	}
	/**
	 * metodo che definisce il comportamento dell'entit� quando questa muore
	 */
	public void die() {
		if(energy <= 0) {
			setCategoryFilter(JavaBomber.DESTROYED_BIT);
			controller.removeEntity(this);
		}
		else energy--;
	}
	

}
