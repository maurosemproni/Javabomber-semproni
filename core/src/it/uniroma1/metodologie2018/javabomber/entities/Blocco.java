package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.interfaces.Block;

/**
 * Superclasse che permette di definire i blocchi della mappa
 * @author Mauro
 *
 */
public abstract class Blocco implements Block {
	/**
	 * il controller
	 */
	protected WorldController controller;
	/**
	 * la posizione del blocco
	 */
	private Vector2 pos;
	
	/**
	 * Costruttore della classe 
	 * @param controller il controller
	 * @param pos la posizione 
	 */
	public Blocco(WorldController controller, Vector2 pos) {
		this.controller = controller;
		this.pos = pos;
	}

	/**
	 * metodo che implementa la distruzione del blocco
	 */
	public void destroy() {
		controller.removeEntity(this);
	}
	
	@Override
	public Vector2 getPosition() {
		return pos;
	}
}
