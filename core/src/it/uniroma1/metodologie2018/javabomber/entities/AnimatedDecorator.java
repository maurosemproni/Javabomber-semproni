package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Superclasse necessaria all'implementazione del Decorator Pattern che gestisce le animazioni delle entit�
 * @author Mauro
 *
 */
abstract public class AnimatedDecorator extends Animated {

	/**
	 * Componente da decorare
	 */
	protected Animated animated;
	protected Sprite sprite;
	
	/**
	 * Costruttore della classe
	 * @param animated il componente da decorare
	 */
	public AnimatedDecorator(Animated animated) {
		this.animated = animated;
	}
	
	/**
	 * Fornisce l'animaizone. Deve essere implementato nell'update dell'entit� che usa il Decorator
	 * @param deltaTime il delta time fornito in input al metodo update della classe che usa il decorator
	 */
	abstract public void animate(float deltaTime);
	
	public Sprite getSprite() { 
		return sprite;
	}

}
