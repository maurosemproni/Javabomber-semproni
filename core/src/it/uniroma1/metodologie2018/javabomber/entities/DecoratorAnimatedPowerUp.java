package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;

/**
 * Classe che definisce l'aspetto dei power up
 * @author Mauro
 *
 */
public class DecoratorAnimatedPowerUp extends AnimatedDecorator {

	/**
	 * TextureAtlas usato per i power up
	 */
	public static final TextureAtlas  ATLAS = Config.POWER_UP_PACK;
	
	/**
	 * Enum che elenca tutti i possibili power up
	 * @author Mauro
	 *
	 */
	public enum PowerUpAspect {
		
		PORTAL(ATLAS.findRegion("portal")),
		LIFE_UP(ATLAS.findRegion("life_up")),
		SPEED_UP(ATLAS.findRegion("speed_up")),
		SPEED_DOWN(ATLAS.findRegion("speed_down")),
		TIME_UP(ATLAS.findRegion("time_up")),
		TIME_DOWN(ATLAS.findRegion("time_down")),
		EXPLOSION_UP(ATLAS.findRegion("explosion_up")),
		EXPLOSION_DOWN(ATLAS.findRegion("explosion_down")),
		BOMB_UP(ATLAS.findRegion("bomb_up")),
		BOMB_DOWN(ATLAS.findRegion("bomb_down")),
		WALK_THROUGH_BOMB(ATLAS.findRegion("bomb_not_collide")),
		KICK_BOMB(ATLAS.findRegion("kick_bomb")),
		CARRY_BOMB(ATLAS.findRegion("bomb_carry")),
		RANDOM_UP(ATLAS.findRegion("random_up")),
		RANDOM_DOWN(ATLAS.findRegion("random_down")),
		EARTHQUAKE(ATLAS.findRegion("earthquake")),
		SHOOT(ATLAS.findRegion("shoot"));
		
		/**
		 * TextureRegion corrispondente al power up all'interno del TextureAtlas
		 */
		private TextureRegion region;
		
		/**
		 * Costruttore della enum
		 * @param region
		 */
		PowerUpAspect(TextureRegion region) {
			this.region = region;
		}
		
		/**
		 * Metodo get per il campo region
		 * @return il valore attuale del campo region
		 */
		public TextureRegion getRegion() {
			return region;
		}
		
	}
	
	/**
	 * il power up da decorare
	 */
	private GameEntity animated;
	/**
	 * l'aspetto grafico del power up
	 */
	private PowerUpAspect aspect;
	
	/**
	 * Costruttore della classe
	 * @param animated il power up da decorare
	 * @param aspect l'aspetto del power up
	 */
	public DecoratorAnimatedPowerUp(GameEntity animated, PowerUpAspect aspect) {
		super(animated);
		sprite = new Sprite(aspect.getRegion(), JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE, JavaBomber.TILE_SIZE);
		sprite.setOrigin(sprite.getWidth()/2.f, sprite.getHeight()/2.f);
		sprite.setPosition(animated.getPosition().x, animated.getPosition().y);
		sprite.setRegion(aspect.getRegion());
	}
	
	@Override
	public void animate(float deltaTime) {
	}
}
