package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.interfaces.Enemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * Nemico semplice che si muove randomicamente e che uccide Bomberman qualora ci collida
 * @author Mauro
 *
 */
public class SimpleEnemy extends GameEntity implements Enemy {
	
	/**
	 * Builder per la classe SimpleEnemy
	 * @author Mauro
	 *
	 */
	public static class SimpleEnemyBuilder extends GameEntityBuilder<SimpleEnemyBuilder>{
		
		@Override
		protected SimpleEnemyBuilder self() {
			return this;
		}
		
		/**
		 * metodo che costruisce l'entit� con le caratteristiche selezionate
		 */
		public SimpleEnemy build() {
			SimpleEnemy instance = new SimpleEnemy();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.animation = new DecoratorAnimatedEntity(instance, AnimationState.STANDING, new TextureAtlas(Gdx.files.internal("enemy_armor.pack")));
			instance.defineEntity();
			instance.movement = new RandomMovement(instance.controller, instance.body);
			return instance;
		}

	}
	
	/**
	 * Decorator per l'animazione
	 */
	private DecoratorAnimatedEntity animation;
	/**
	 * Oggetto che definisce il movimento
	 */
	private Movement movement;


	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void collide(Entity e) {
		if(e instanceof Explosion) die();
		if(e instanceof Blocco) movement.changeDirection();
		if(e instanceof Bomb) movement.changeDirection();
	}


	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + animation.getSprite().getWidth() / 2, pos.y + animation.getSprite().getHeight() / 2);
		bdef.type = BodyDef.BodyType.DynamicBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(animation.getSprite().getRegionWidth() / 2 - JavaBomber.TOLERANCE, animation.getSprite().getRegionHeight() / 3 - JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.ENEMY_BIT;
		fdef.filter.maskBits = JavaBomber.BOMB_BIT | JavaBomber.BLOCK_BIT | JavaBomber.EXPLOSION_BIT;
		fdef.shape = shape;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);
		animation.animate(deltaTime);
		pos = body.getPosition();
		controller.getWorld().step(deltaTime, 6, 2);
		movement.move();
		animation.getSprite().setPosition(body.getPosition().x - animation.getSprite().getWidth() / 2, body.getPosition().y - animation.getSprite().getHeight() / 3);
	}
	
	@Override
	public void draw(Batch batch) {
		animation.getSprite().draw(batch);
	}
	
	/**
	 * Definisce il comportamento dell'entit� quando questa muore
	 */
	public void die() {
		setCategoryFilter(JavaBomber.DESTROYED_BIT);
		controller.removeEntity(this);
	}
}
