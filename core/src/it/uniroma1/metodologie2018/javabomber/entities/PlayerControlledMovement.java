package it.uniroma1.metodologie2018.javabomber.entities;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;

/**
 * Definisce il movimento controllato dall'utente
 * @author Mauro
 *
 */
public class PlayerControlledMovement implements Movement, InputProcessor {

	
	/**
	 * Enum che definisce la direzione del personaggio
	 * @author Federico Scozzafava
	 *
	 */
	protected enum Direction{
		UP(new Vector2(0,1)), DOWN(new Vector2(0,-1)), LEFT(new Vector2(-1,0)), RIGHT(new Vector2(1,0)), IDLE(new Vector2(0,0));

		private Vector2 p;
		Direction(Vector2 p)
		{
			this.p = p;
		}

		public Vector2 getDirVector()
		{
			return p;
		}
	}
	
	/**
	 * la velocit� minima
	 */
	public static final float SPEED_MIN = 50.0f;
	/**
	 * la velocit� massima
	 */
	public static final float SPEED_MAX = 600.0f;
	/**
	 * la velocit� del movimento
	 */
	private float speed;
	/**
	 * la direzione del movimento
	 */
	private Direction direction = Direction.IDLE;
	/**
	 * il body al quale agganciare il movimento
	 */
	private Body body;
	/**
	 * riferimento all'entit� che implementa questo movimento
	 */
	private GameEntity player;

	/**
	 * Costruttore della classe
	 * @param player l'entit� alla quale agganciare il movimento
	 */
	public PlayerControlledMovement(GameEntity player) {
		this.player = player;
		this.body = player.getBody();
		speed = SPEED_MIN;
	}
	
	@Override
	public void move() {
		body.setLinearVelocity(direction.getDirVector().x * speed, direction.getDirVector().y * speed);
	}
	
	/**
	 * Posiziona l'enti� al centro del tile in cui si trova
	 */
	private void centerToTile() {
		Vector2 result = new Vector2();
		float deltaX = (body.getPosition().x - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		float deltaY = (body.getPosition().y - JavaBomber.TILE_SIZE / 2) % JavaBomber.TILE_SIZE;
		result.x = deltaX < 16 ? body.getPosition().x - deltaX : body.getPosition().x + JavaBomber.TILE_SIZE - deltaX;
		result.y = deltaY < 16 ? body.getPosition().y - deltaY : body.getPosition().y + JavaBomber.TILE_SIZE - deltaY;
		body.setTransform(result, 0);
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode)
		{
		case Keys.SPACE: player.mainAction();break;
		case Keys.UP: {
			centerToTile();
			direction = Direction.UP; break;
		}
		case Keys.DOWN: {
			centerToTile();
			direction = Direction.DOWN; break;
		}
		case Keys.LEFT: {
			centerToTile();
			direction = Direction.LEFT; break;
		}
		case Keys.RIGHT: direction = Direction.RIGHT; break;
		default: {
			centerToTile();
			direction = Direction.IDLE;
		}
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(	(direction == Direction.UP && keycode == Keys.UP) ||
			(direction == Direction.DOWN && keycode == Keys.DOWN) ||
			(direction == Direction.LEFT && keycode == Keys.LEFT) ||
			(direction == Direction.RIGHT && keycode == Keys.RIGHT)) {
				direction = Direction.IDLE;
				centerToTile();
		}
		return true;
	}
	
	@Override
	public void SpeedUp() {
		speed += speed < SPEED_MAX ? SPEED_MIN : 0;
	}
	
	@Override
	public void SpeedDown() {
		speed -= speed > SPEED_MIN ? SPEED_MIN : 0;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public Direction getDirection() { return direction; }
	
	@Override
	public float getSpeed() { return speed; }

	@Override
	public void think() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeDirection() {
		// TODO Auto-generated method stub
		
	}

}
