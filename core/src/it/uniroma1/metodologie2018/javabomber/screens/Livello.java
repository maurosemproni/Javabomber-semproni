package it.uniroma1.metodologie2018.javabomber.screens;

import it.uniroma1.metodologie2018.javabomber.interfaces.Level;
import it.uniroma1.metodologie2018.javabomber.interfaces.Map;

public class Livello implements Level {
	private Map mappa;
	
	public Livello(Map mappa) {
		this.mappa = mappa;
	}
	
	@Override
	public Map getMap() {
		return mappa;
	}

}
