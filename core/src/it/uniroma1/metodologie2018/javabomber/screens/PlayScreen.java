package it.uniroma1.metodologie2018.javabomber.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.Config;
import it.uniroma1.metodologie2018.javabomber.entities.Terminator;
import it.uniroma1.metodologie2018.javabomber.entities.EntitiesContactListener;
import it.uniroma1.metodologie2018.javabomber.entities.PlayerControlledMovement;
import it.uniroma1.metodologie2018.javabomber.entities.SimpleEnemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.Block;
import it.uniroma1.metodologie2018.javabomber.interfaces.Enemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.GameScreen;
import it.uniroma1.metodologie2018.javabomber.interfaces.Level;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldRenderer;
import it.uniroma1.metodologie2018.javabomber.maps.Mappa;
import it.uniroma1.metodologie2018.javabomber.powerup.Earthquake;
import it.uniroma1.metodologie2018.javabomber.powerup.Portal;
import it.uniroma1.metodologie2018.javabomber.powerup.PowerUpSpawner;
import it.uniroma1.metodologie2018.javabomber.world.JavaBomberWorldController;
import it.uniroma1.metodologie2018.javabomber.world.JavaBomberWorldRenderer;

public class PlayScreen implements GameScreen{

	private WorldController worldController;
	private WorldRenderer worldRenderer;
	private Bomberman bomberman;
	private Enemy enemy1;
	private Terminator boss;
	private boolean bEarthquake;
	
	private Level livello;
	

	private boolean paused = false;

	public PlayScreen(JavaBomber game, int lives) {
		worldController = new JavaBomberWorldController(game);
		worldRenderer = new JavaBomberWorldRenderer(game,worldController);
		livello = new Livello(new Mappa(worldController));
		worldController.setLevel(livello);
		
		bomberman = (new Bomberman.BombermanBuilder())
				.setWorldController(worldController)
				.setPosition(new Vector2(32,320))
				.setLives(lives)
				.build(); 

		enemy1 = new SimpleEnemy.SimpleEnemyBuilder()
				.setWorldController(worldController)
				.setPosition(new Vector2(160,32))
				.build();
		
		boss = new Terminator.BossBuilder()
				.setWorldController(worldController)
				.setPosition(new Vector2(416,128))
				.setBombStock(2)
				.setEnergy(10)
				.build();

		
		worldController.getWorld().setContactListener(new EntitiesContactListener());
		Config.setScreen(this);
		
		init();
	}

	private void init()
	{
		worldController.addEntity(bomberman);
		worldController.addEntity(enemy1);
		//worldController.addEntity(boss);
		for(Block b : livello.getMap().getBlocks()) worldController.addEntity(b); // AGGIUNTO
	}

	@Override
	public void render(float delta) {
		if(!paused)
		{
			worldController.update(delta);
		}
		Gdx.gl.glClearColor(0.2f, 0.5f, 0.3f, 0);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		((Mappa)livello.getMap()).getMapRenderer().setView(worldRenderer.getCamera()); // AGGIUNTO
		((Mappa)livello.getMap()).getMapRenderer().render(); // AGGIUNTO
		worldRenderer.getCamera().update(); //AGGIUNTO
		worldRenderer.render();
		
		if(worldController.getEntities().stream().filter(e -> e instanceof Enemy).count() <= 0)
			worldController.addEntity(new Portal.PortalBuilder().setWorldController(worldController).setPosition(new Vector2(224,224)).build());
		
		if(bEarthquake) Earthquake.shake(delta, worldRenderer.getCamera(), this);
	}
	
	public void startEarthquake() { bEarthquake = true; }
	
	public void stopEarthquake() { bEarthquake = false; }

	@Override
	public void resize(int width, int height) {
		worldRenderer.resize(width, height);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void resume() {
		paused = false;
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		worldController.dispose();
		worldRenderer.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {
		//player.keyDown(keycode);
		if(bomberman.getMovement() instanceof PlayerControlledMovement) { // forse � il caso di inserire getMovement() nell'interfaccia GraphicEntity
			PlayerControlledMovement movement = (PlayerControlledMovement) bomberman.getMovement();
			movement.keyDown(keycode);
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		//player.keyUp(keycode);
		if(bomberman.getMovement() instanceof PlayerControlledMovement) {
			PlayerControlledMovement movement = (PlayerControlledMovement) bomberman.getMovement();
			movement.keyUp(keycode);
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
