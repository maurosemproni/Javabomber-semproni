package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.entities.GameEntity.GameEntityBuilder;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.powerup.ExplosionDown.ExplosionDownBuilder;

/**
 * Power Up che aumenta l'estensione delle esplosioni
 * @author Mauro
 *
 */
public class ExplosionUp extends PowerUp{
	
	/**
	 * Builder per la classe ExplosionUp
	 * @author Mauro
	 *
	 */
	public static class ExplosionUpBuilder extends GameEntityBuilder<ExplosionUpBuilder>{
		
		@Override
		protected ExplosionUpBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public ExplosionUp build() {
			ExplosionUp instance = new ExplosionUp();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.EXPLOSION_UP);
			instance.defineEntity();
			return instance;
		}

	}

}
