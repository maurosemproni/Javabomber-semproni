package it.uniroma1.metodologie2018.javabomber.powerup;

import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;

/**
 * Power Up che diminuisce la velocit�
 * @author Mauro
 *
 */
public class SpeedDown extends PowerUp {
	
	/**
	 * Builder per la classe SpeedDown
	 * @author Mauro
	 *
	 */
	public static class SpeedDownBuilder extends GameEntityBuilder<SpeedDownBuilder>{
		
		@Override
		protected SpeedDownBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public SpeedDown build() {
			SpeedDown instance = new SpeedDown();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.SPEED_DOWN);
			instance.defineEntity();
			return instance;
		}

	}
}
