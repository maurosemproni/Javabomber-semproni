package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.entities.GameEntity.GameEntityBuilder;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.powerup.BombDown.BombDownBuilder;

/**
 * Power Up che aumenta il numero delle bombe che � possibile piazzare contemporaneamente
 * @author Mauro
 *
 */
public class BombUp extends PowerUp {
	
	/**
	 * Builder per la classe BombUp
	 * @author Mauro
	 *
	 */
	public static class BombUpBuilder extends GameEntityBuilder<BombUpBuilder>{
		
		@Override
		protected BombUpBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public BombUp build() {
			BombUp instance = new BombUp();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.BOMB_UP);
			instance.defineEntity();
			return instance;
		}

	}

}
