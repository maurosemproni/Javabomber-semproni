package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Bomb;
import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.Config;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.Explosion;
import it.uniroma1.metodologie2018.javabomber.entities.GameEntity;
import it.uniroma1.metodologie2018.javabomber.entities.Bomb.BombBuilder;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedEntity.AnimationState;
import it.uniroma1.metodologie2018.javabomber.entities.GameEntity.GameEntityBuilder;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

/**
 * Classe che definisce i PowerUp
 * @author Mauro
 *
 */
public abstract class PowerUp extends GameEntity {
	
	/**
	 * Durata del timer
	 */
	public static final int TIMER_DURATION = 10000;
	/**
	 * decorator che definisce l'aspetto grafico del power up
	 */
	protected DecoratorAnimatedPowerUp aspect;


	@Override
	public void setPosition(Vector2 pos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + JavaBomber.TILE_SIZE / 2, pos.y + JavaBomber.TILE_SIZE  / 2);
		bdef.type = BodyDef.BodyType.StaticBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(JavaBomber.TILE_SIZE  / 2 - 2*JavaBomber.TOLERANCE, JavaBomber.TILE_SIZE  / 2 - 2*JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.POWER_UP_BIT;
		fdef.filter.maskBits = JavaBomber.PLAYER_BIT | JavaBomber.EXPLOSION_BIT;
		fdef.shape = shape;
		fdef.isSensor = true;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}

	@Override
	public void collide(Entity e) {
		if(e instanceof Bomberman) destroy();
		if(e instanceof Explosion) destroy();
		
	}
	
	/**
	 * Defisice il comportamento del power up quando deve essere distrutto
	 */
	private void destroy() {
		controller.removeEntity(this);
		setCategoryFilter(JavaBomber.DESTROYED_BIT);
	}
	
	/**
	 * disegna a schermo il power up
	 */
	public void draw(Batch batch) {
		aspect.getSprite().draw(batch);
	}
	
	@Override
	public int getZindex() {
		return Config.POWER_UP_Z_INDEX;
	}

}
