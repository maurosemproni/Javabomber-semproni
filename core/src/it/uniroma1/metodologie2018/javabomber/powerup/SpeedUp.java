package it.uniroma1.metodologie2018.javabomber.powerup;

import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;


/**
 * Power up che aumenta la velocit�
 * @author Mauro
 *
 */
public class SpeedUp extends PowerUp {
	
	/**
	 * Builder per la classe SpeedUp
	 * @author Mauro
	 *
	 */
	public static class SpeedUpBuilder extends GameEntityBuilder<SpeedUpBuilder>{
		
		@Override
		protected SpeedUpBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public SpeedUp build() {
			SpeedUp instance = new SpeedUp();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.SPEED_UP);
			instance.defineEntity();
			return instance;
		}

	}

}
