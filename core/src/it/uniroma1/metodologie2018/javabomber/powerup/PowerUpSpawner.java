package it.uniroma1.metodologie2018.javabomber.powerup;

import java.util.Random;

import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;

/**
 * Si occupa di spargere i power up per la mappa in modo casuale
 * @author Mauro
 *
 */
public class PowerUpSpawner {
	private WorldController controller;
	
	/**
	 * Costruttore della calsse
	 * @param controller riferimento la WorldController
	 */
	public PowerUpSpawner(WorldController controller) {
		this.controller = controller;
	}
	
	/**
	 * metodo che crea casulamente un powerUp
	 * @param pos la posizione in cui il power up deve essere creato
	 * @return un power up generato casualmente
	 */
	public PowerUp spawn(Vector2 pos) {
		PowerUp result;
		pos = new Vector2(pos.x - JavaBomber.TILE_SIZE / 2, pos.y - JavaBomber.TILE_SIZE / 2);
			
		Random rand = new Random();
		int val = rand.nextInt(40);
		//val = 8;
		switch(val) {
		case 0:	result = new ExplosionUp.ExplosionUpBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 1:	result = new ExplosionDown.ExplosionDownBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 2: result = new BombUp.BombUpBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 3:	result = new BombDown.BombDownBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 4: result = new SpeedUp.SpeedUpBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 5: result = new SpeedDown.SpeedDownBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 6: result = new LifeUp.LifeUpBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 7: result = new WalkThroughBombs.WalkThroughBombsBuilder().setWorldController(controller).setPosition(pos).build(); break;
		case 8: result = new Earthquake.EarthquakeBuilder().setWorldController(controller).setPosition(pos).build(); break;
		
		default: result = null; break;
		}
			
		return result;
	}
	
}
