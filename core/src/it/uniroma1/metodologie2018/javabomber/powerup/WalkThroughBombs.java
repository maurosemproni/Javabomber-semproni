package it.uniroma1.metodologie2018.javabomber.powerup;

import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * PowerUp che, una volta raccolto, permette di non collidere con le bombe per 10 secondi.
 * @author Mauro
 *
 */
public class WalkThroughBombs extends PowerUp {
	
	/**
	 * Builder per la classe WalkThroughBombs
	 * @author Mauro
	 *
	 */
	public static class WalkThroughBombsBuilder extends GameEntityBuilder<WalkThroughBombsBuilder>{
		/**
		 * riferimento all'entit� che ha raccolto il powerUp
		 */
		private Bomberman owner;
		
		
		@Override
		protected WalkThroughBombsBuilder self() {
			return this;
		}
		
		/**
		 * Metodo set per il campo owner
		 * @param owner Entit� che ha raccolto il powerUp
		 * @return this
		 */
		public WalkThroughBombsBuilder setOwner(Bomberman owner) {
			this.owner = owner;
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public WalkThroughBombs build() {
			WalkThroughBombs instance = new WalkThroughBombs();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.WALK_THROUGH_BOMB);
			instance.defineEntity();
			return instance;
		}

	}
	
	
	/**
	 * Riferimento all'entit� che ha raccolto il PowerUp
	 */
	private Bomberman owner;

	/**
	 * Fa partire il timer del power up
	 */
	public void startTimer() {
		new java.util.Timer().schedule(
			new java.util.TimerTask() {
				@Override
				public void run() {
					owner.walkThroughBombsToggle();
				}
			}, TIMER_DURATION
		);
	}
	
	@Override
	public void collide(Entity e) {
		super.collide(e);
		if(e instanceof Bomberman) {
			owner = (Bomberman) e;
			owner.walkThroughBombsToggle();
			startTimer();
		}
	}

}
