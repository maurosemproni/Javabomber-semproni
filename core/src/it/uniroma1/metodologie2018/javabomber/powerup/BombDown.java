package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.graphics.g2d.Batch;

import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;


/**
 * Power Up che diminuisce il numero delle bombe che si possono piazzare contemporaneamente
 * @author Mauro
 *
 */
public class BombDown extends PowerUp{
	
	/**
	 * Builder per la classe BombDown
	 * @author Mauro
	 *
	 */
	public static class BombDownBuilder extends GameEntityBuilder<BombDownBuilder>{
		
		@Override
		protected BombDownBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le psecifiche scelte
		 */
		public BombDown build() {
			BombDown instance = new BombDown();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.BOMB_DOWN);
			instance.defineEntity();
			return instance;
		}

	}
	
}
