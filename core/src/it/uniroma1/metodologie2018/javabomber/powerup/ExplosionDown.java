package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;

import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.entities.GameEntity.GameEntityBuilder;
import it.uniroma1.metodologie2018.javabomber.interfaces.WorldController;
import it.uniroma1.metodologie2018.javabomber.powerup.BombUp.BombUpBuilder;

/**
 * Power up che diminuisce l'estensione delle esplosioni
 * @author Mauro
 *
 */
public class ExplosionDown extends PowerUp {
	
	/**
	 * Builder per la calsse ExplosionDown
	 * @author Mauro
	 *
	 */
	public static class ExplosionDownBuilder extends GameEntityBuilder<ExplosionDownBuilder>{
		
		@Override
		protected ExplosionDownBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public ExplosionDown build() {
			ExplosionDown instance = new ExplosionDown();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.EXPLOSION_DOWN);
			instance.defineEntity();
			return instance;
		}

	}

}
