package it.uniroma1.metodologie2018.javabomber.powerup;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;
import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.interfaces.Enemy;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;

/**
 * Porta di fine livello
 * @author Mauro
 *
 */
public class Portal extends PowerUp {
	
	/**
	 * Builder per la classe Portal
	 * @author Mauro
	 *
	 */
	public static class PortalBuilder extends GameEntityBuilder<PortalBuilder>{
	
		@Override
		protected PortalBuilder self() {
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public Portal build() {
			Portal instance = new Portal();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.PORTAL);
			instance.defineEntity();
			return instance;
		}

	}
	
	@Override
	public void defineEntity() {
		BodyDef bdef = new BodyDef();
		bdef.position.set(pos.x + JavaBomber.TILE_SIZE / 2, pos.y + JavaBomber.TILE_SIZE  / 2);
		bdef.type = BodyDef.BodyType.StaticBody;
		body = controller.getWorld().createBody(bdef);
		FixtureDef fdef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(JavaBomber.TILE_SIZE  / 2 - 2*JavaBomber.TOLERANCE, JavaBomber.TILE_SIZE  / 2 - 2*JavaBomber.TOLERANCE);
		fdef.filter.categoryBits = JavaBomber.POWER_UP_BIT;
		fdef.filter.maskBits = JavaBomber.PLAYER_BIT;
		fdef.shape = shape;
		fdef.isSensor = true;
		fixture = body.createFixture(fdef);
		fixture.setUserData(this);
	}
	
	@Override
	public void collide(Entity e) {

		if(e instanceof Bomberman) {
			// Condizione di vittoria
			System.out.println("=========== HAI VINTO! ===========");
		}
	}

}
