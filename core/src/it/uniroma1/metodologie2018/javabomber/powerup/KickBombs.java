package it.uniroma1.metodologie2018.javabomber.powerup;

import it.uniroma1.metodologie2018.javabomber.entities.Bomberman;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;
import it.uniroma1.metodologie2018.javabomber.interfaces.Entity;


/**
 * Power up che permette di calciare le bombe
 * @author Mauro
 *
 */
public class KickBombs extends PowerUp {
	
	/**
	 * Builder per la classe KickBombs
	 * @author Mauro
	 *
	 */
	public static class KickBombsBuilder extends GameEntityBuilder<KickBombsBuilder>{
		/**
		 * riferimento all'entit� che ha raccolto il powerUp
		 */
		private Bomberman owner;
		
		
		@Override
		protected KickBombsBuilder self() {
			return this;
		}
		
		/**
		 * metodo set per il campo owner
		 * @param owner l'entit� che ha raccolto il powerUp
		 * @return this
		 */
		public KickBombsBuilder setOwner(Bomberman owner) {
			this.owner = owner;
			return this;
		}
		
		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public KickBombs build() {
			KickBombs instance = new KickBombs();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.KICK_BOMB);
			instance.owner = owner;
			instance.defineEntity();
			return instance;
		}

	}
	
	/**
	 * la durata del PowerUp
	 */
	private static final int TIMER_DURATION = 10000;
	/**
	 * riferimento all'entit� che ha raccolto il PowerUp
	 */
	private Bomberman owner;

	/**
	 * fa partire il timer del PowerUp
	 */
	public void startTimer() {
		new java.util.Timer().schedule(
			new java.util.TimerTask() {
				@Override
				public void run() {
					owner.kickBombsToggle();
				}
			}, TIMER_DURATION
		);
	}

	
	@Override
	public void collide(Entity e) {
		super.collide(e);
		if(e instanceof Bomberman) {
			owner = (Bomberman) e;
			owner.kickBombsToggle();
			startTimer();
		}
	}
}
