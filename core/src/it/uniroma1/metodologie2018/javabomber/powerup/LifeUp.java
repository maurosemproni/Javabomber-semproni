package it.uniroma1.metodologie2018.javabomber.powerup;

import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp;
import it.uniroma1.metodologie2018.javabomber.entities.DecoratorAnimatedPowerUp.PowerUpAspect;


/**
 * Power Up che aumenta le vite del personaggio
 * @author Mauro
 *
 */
public class LifeUp extends PowerUp {
	
	/**builder per la classe LifeUp
	 * @author Mauro
	 *
	 */
	public static class LifeUpBuilder extends GameEntityBuilder<LifeUpBuilder>{
		
		@Override
		protected LifeUpBuilder self() {
			return this;
		}

		/**
		 * Metodo che restituisce il PowerUp con le specifiche scelte
		 */
		public LifeUp build() {
			LifeUp instance = new LifeUp();
			instance.controller = this.controller;
			instance.pos = this.pos;
			instance.aspect = new DecoratorAnimatedPowerUp(instance, PowerUpAspect.LIFE_UP);
			instance.defineEntity();
			return instance;
		}

	}
}
