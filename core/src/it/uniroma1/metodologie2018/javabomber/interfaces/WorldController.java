package it.uniroma1.metodologie2018.javabomber.interfaces;

import java.util.List;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;

import it.uniroma1.metodologie2018.javabomber.JavaBomber;

public interface WorldController extends InputProcessor,Disposable{
	void update(float deltaTime);
	
	void addEntity(Entity e);
	
	void removeEntity(Entity e);
	
	List<Entity> getEntities();
	
	Level getLevel();
	
	void setLevel(Level currentLevel);
	
	World getWorld(); //AGGIUNTO
	
	void playerDead(int lives); //AGGIUNTO

}