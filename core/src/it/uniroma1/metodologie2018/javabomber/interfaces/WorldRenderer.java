package it.uniroma1.metodologie2018.javabomber.interfaces;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.Disposable;


public interface WorldRenderer extends Disposable{
	void render();

	void resize(int width, int height);
	
	OrthographicCamera getCamera(); //AGGIUNTO

}
