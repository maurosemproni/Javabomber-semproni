package it.uniroma1.metodologie2018.javabomber.interfaces;

import java.util.List;

import com.badlogic.gdx.math.Vector2;

public interface Map {
	void render();
	
	void init();
	
	void update(float deltaTime);
	
	void reset();
	
	Vector2 getSize();
	
	List<Block> getBlocks();
	
	void putEntity(Entity e, Vector2 pos);
	
	Entity getBlock(Vector2 pos);
	
	Entity getEntity(Vector2 pos);
}
