package it.uniroma1.metodologie2018.javabomber.interfaces;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public interface Entity{
	void update(float deltaTime);
	
	Vector2 getPosition();
	
	void setPosition(Vector2 pos);
	
	Body getBody();
	
	void collide(Entity e);
	
	int getZindex();
}
