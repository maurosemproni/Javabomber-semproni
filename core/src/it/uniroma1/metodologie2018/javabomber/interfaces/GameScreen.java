package it.uniroma1.metodologie2018.javabomber.interfaces;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;

public interface GameScreen extends Screen, InputProcessor{

}
