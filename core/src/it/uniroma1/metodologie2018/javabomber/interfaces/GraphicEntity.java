package it.uniroma1.metodologie2018.javabomber.interfaces;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface GraphicEntity extends Entity{
	void draw(Batch batch);
}
