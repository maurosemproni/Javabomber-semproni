package it.uniroma1.metodologie2018.javabomber.interfaces;

/**
 * Interfaccia implementata da tutti i personaggi che possono piazzare bombe
 * @author Mauro
 *
 */
public interface BombDropper {
	
	/**
	 * piazza la bomba
	 */
	void dropBomb();
	
	/**
	 * restituisce l'estensione dell'esplosione
	 * @return un intero che rappresenta l'estensione dell'esplosione in tiles
	 */
	int getExplosionExtent();
	
	/**
	 * resetta il conteggio delle bombe
	 */
	void restoreBombCounter();

}
