package it.uniroma1.metodologie2018.javabomber;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import it.uniroma1.metodologie2018.javabomber.entities.Config;
import it.uniroma1.metodologie2018.javabomber.interfaces.GameScreen;
import it.uniroma1.metodologie2018.javabomber.screens.PlayScreen;

public class JavaBomber extends Game {

	private Batch batch;
	public static final float PPM = 100;
	/**
	 * Dimensione del tile della mappa, serve da riferimento anche per i body delle entit�
	 */
	public static final int TILE_SIZE = 32;
	/**
	 * Margine di tolleranza per la grandezza dei body, introdotto perch� Tiled non � molto preciso nella realizzazione delle shape
	 */
	public static final int TOLERANCE = 2;
	
	public static final short DEFAULT_BIT = 1;
	public static final short PLAYER_BIT = 2;
	public static final short BOMB_BIT = 4;
	public static final short BLOCK_BIT = 8;
	public static final short ENEMY_BIT = 16;
	public static final short DESTROYED_BIT = 32;
	public static final short EXPLOSION_BIT = 64;
	public static final short POWER_UP_BIT = 128;
	public static final short BOMB_JUST_PLACED_BIT = 256;

	@Override
	public void create () {
		Config.setGame(this);
		// Set Libgdx log level
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		batch = new SpriteBatch();
		setScreen(new PlayScreen(this, 3));
	}

	@Override
	public void setScreen(Screen screen) {
		super.setScreen(screen);
		if(screen instanceof GameScreen)
		{
			GameScreen newScreen = (GameScreen)screen;
			Gdx.input.setInputProcessor(newScreen);
		}
		else
			Gdx.input.setInputProcessor(null);
	}

	public Batch getBatch() {
		return batch;
	}

	@Override
	public void dispose() {
		batch.dispose();
		super.dispose();
	}

}
